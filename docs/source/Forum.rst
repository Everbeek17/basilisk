News and Discussions
====================


.. note::

    A Google Talk Forum is setup to search or post Basilisk related questions.  The link to the forum is:

    - `<https://groups.google.com/forum/embed/?place=forum%2Fbasilisk-forum>`__

.. note::

    A Facebook page is used to post news about Basilisk and Vizard.  The link to the page is:

    - `<http://facebook.com/basiliskAstro>`__

.. note::

    A YouTube channel is setup where all the Basilisk related tutorial and instructional videos can be found:

    - `<https://www.youtube.com/playlist?list=PLi0t2uvz2LtgCSYTsqz1gxRJU4YD0Yqjk>`__
