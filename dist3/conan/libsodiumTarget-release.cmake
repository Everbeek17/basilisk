
set(libsodium_INCLUDE_DIRS_RELEASE "C:/.conan/c116dd/1/include")
set(libsodium_INCLUDE_DIR_RELEASE "C:/.conan/c116dd/1/include")
set(libsodium_INCLUDES_RELEASE "C:/.conan/c116dd/1/include")
set(libsodium_RES_DIRS_RELEASE )
set(libsodium_DEFINITIONS_RELEASE )
set(libsodium_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(libsodium_COMPILE_DEFINITIONS_RELEASE )
set(libsodium_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(libsodium_COMPILE_OPTIONS_C_RELEASE "")
set(libsodium_COMPILE_OPTIONS_CXX_RELEASE "")
set(libsodium_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(libsodium_LIBRARIES_RELEASE "") # Will be filled later
set(libsodium_LIBS_RELEASE "") # Same as libsodium_LIBRARIES
set(libsodium_SYSTEM_LIBS_RELEASE )
set(libsodium_FRAMEWORK_DIRS_RELEASE )
set(libsodium_FRAMEWORKS_RELEASE )
set(libsodium_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(libsodium_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(libsodium_FRAMEWORKS_FOUND_RELEASE "${libsodium_FRAMEWORKS_RELEASE}" "${libsodium_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(libsodium_INCLUDE_DIRS_RELEASE
                 libsodium_INCLUDE_DIR_RELEASE
                 libsodium_INCLUDES_RELEASE
                 libsodium_DEFINITIONS_RELEASE
                 libsodium_LINKER_FLAGS_RELEASE_LIST
                 libsodium_COMPILE_DEFINITIONS_RELEASE
                 libsodium_COMPILE_OPTIONS_RELEASE_LIST
                 libsodium_LIBRARIES_RELEASE
                 libsodium_LIBS_RELEASE
                 libsodium_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to libsodium_LIBS and libsodium_LIBRARY_LIST
set(libsodium_LIBRARY_LIST_RELEASE libsodium)
set(libsodium_LIB_DIRS_RELEASE "C:/.conan/c116dd/1/lib")

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_libsodium_DEPENDENCIES_RELEASE "${libsodium_FRAMEWORKS_FOUND_RELEASE} ${libsodium_SYSTEM_LIBS_RELEASE} ")

conan_package_library_targets("${libsodium_LIBRARY_LIST_RELEASE}"  # libraries
                              "${libsodium_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_libsodium_DEPENDENCIES_RELEASE}"  # deps
                              libsodium_LIBRARIES_RELEASE            # out_libraries
                              libsodium_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "libsodium")                                      # package_name

set(libsodium_LIBS_RELEASE ${libsodium_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${libsodium_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND libsodium_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND libsodium_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${libsodium_SYSTEM_LIBS_RELEASE})
    list(APPEND libsodium_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND libsodium_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(libsodium_LIBRARIES_TARGETS_RELEASE "${libsodium_LIBRARIES_TARGETS_RELEASE};")
set(libsodium_LIBRARIES_RELEASE "${libsodium_LIBRARIES_RELEASE};")

set(CMAKE_MODULE_PATH "C:/.conan/c116dd/1/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/.conan/c116dd/1/" ${CMAKE_PREFIX_PATH})
