
set(protoc_installer_INCLUDE_DIRS_RELEASE "C:/.conan/227249/1/include")
set(protoc_installer_INCLUDE_DIR_RELEASE "C:/.conan/227249/1/include")
set(protoc_installer_INCLUDES_RELEASE "C:/.conan/227249/1/include")
set(protoc_installer_RES_DIRS_RELEASE )
set(protoc_installer_DEFINITIONS_RELEASE )
set(protoc_installer_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(protoc_installer_COMPILE_DEFINITIONS_RELEASE )
set(protoc_installer_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(protoc_installer_COMPILE_OPTIONS_C_RELEASE "")
set(protoc_installer_COMPILE_OPTIONS_CXX_RELEASE "")
set(protoc_installer_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(protoc_installer_LIBRARIES_RELEASE "") # Will be filled later
set(protoc_installer_LIBS_RELEASE "") # Same as protoc_installer_LIBRARIES
set(protoc_installer_SYSTEM_LIBS_RELEASE )
set(protoc_installer_FRAMEWORK_DIRS_RELEASE )
set(protoc_installer_FRAMEWORKS_RELEASE )
set(protoc_installer_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(protoc_installer_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(protoc_installer_FRAMEWORKS_FOUND_RELEASE "${protoc_installer_FRAMEWORKS_RELEASE}" "${protoc_installer_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(protoc_installer_INCLUDE_DIRS_RELEASE
                 protoc_installer_INCLUDE_DIR_RELEASE
                 protoc_installer_INCLUDES_RELEASE
                 protoc_installer_DEFINITIONS_RELEASE
                 protoc_installer_LINKER_FLAGS_RELEASE_LIST
                 protoc_installer_COMPILE_DEFINITIONS_RELEASE
                 protoc_installer_COMPILE_OPTIONS_RELEASE_LIST
                 protoc_installer_LIBRARIES_RELEASE
                 protoc_installer_LIBS_RELEASE
                 protoc_installer_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to protoc_installer_LIBS and protoc_installer_LIBRARY_LIST
set(protoc_installer_LIBRARY_LIST_RELEASE )
set(protoc_installer_LIB_DIRS_RELEASE )

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_protoc_installer_DEPENDENCIES_RELEASE "${protoc_installer_FRAMEWORKS_FOUND_RELEASE} ${protoc_installer_SYSTEM_LIBS_RELEASE} ")

conan_package_library_targets("${protoc_installer_LIBRARY_LIST_RELEASE}"  # libraries
                              "${protoc_installer_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_protoc_installer_DEPENDENCIES_RELEASE}"  # deps
                              protoc_installer_LIBRARIES_RELEASE            # out_libraries
                              protoc_installer_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "protoc_installer")                                      # package_name

set(protoc_installer_LIBS_RELEASE ${protoc_installer_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${protoc_installer_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND protoc_installer_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND protoc_installer_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${protoc_installer_SYSTEM_LIBS_RELEASE})
    list(APPEND protoc_installer_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND protoc_installer_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(protoc_installer_LIBRARIES_TARGETS_RELEASE "${protoc_installer_LIBRARIES_TARGETS_RELEASE};")
set(protoc_installer_LIBRARIES_RELEASE "${protoc_installer_LIBRARIES_RELEASE};")

set(CMAKE_MODULE_PATH "C:/.conan/227249/1/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/.conan/227249/1/" ${CMAKE_PREFIX_PATH})
