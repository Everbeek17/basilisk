
set(zmq_INCLUDE_DIRS_RELEASE "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/include")
set(zmq_INCLUDE_DIR_RELEASE "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/include")
set(zmq_INCLUDES_RELEASE "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/include")
set(zmq_RES_DIRS_RELEASE )
set(zmq_DEFINITIONS_RELEASE )
set(zmq_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(zmq_COMPILE_DEFINITIONS_RELEASE )
set(zmq_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(zmq_COMPILE_OPTIONS_C_RELEASE "")
set(zmq_COMPILE_OPTIONS_CXX_RELEASE "")
set(zmq_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(zmq_LIBRARIES_RELEASE "") # Will be filled later
set(zmq_LIBS_RELEASE "") # Same as zmq_LIBRARIES
set(zmq_SYSTEM_LIBS_RELEASE )
set(zmq_FRAMEWORK_DIRS_RELEASE )
set(zmq_FRAMEWORKS_RELEASE )
set(zmq_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(zmq_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(zmq_FRAMEWORKS_FOUND_RELEASE "${zmq_FRAMEWORKS_RELEASE}" "${zmq_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(zmq_INCLUDE_DIRS_RELEASE
                 zmq_INCLUDE_DIR_RELEASE
                 zmq_INCLUDES_RELEASE
                 zmq_DEFINITIONS_RELEASE
                 zmq_LINKER_FLAGS_RELEASE_LIST
                 zmq_COMPILE_DEFINITIONS_RELEASE
                 zmq_COMPILE_OPTIONS_RELEASE_LIST
                 zmq_LIBRARIES_RELEASE
                 zmq_LIBS_RELEASE
                 zmq_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to zmq_LIBS and zmq_LIBRARY_LIST
set(zmq_LIBRARY_LIST_RELEASE libzmq-mt-4_2_5.lib ws2_32 Iphlpapi)
set(zmq_LIB_DIRS_RELEASE "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/lib")

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_zmq_DEPENDENCIES_RELEASE "${zmq_FRAMEWORKS_FOUND_RELEASE} ${zmq_SYSTEM_LIBS_RELEASE} libsodium::libsodium")

conan_package_library_targets("${zmq_LIBRARY_LIST_RELEASE}"  # libraries
                              "${zmq_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_zmq_DEPENDENCIES_RELEASE}"  # deps
                              zmq_LIBRARIES_RELEASE            # out_libraries
                              zmq_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "zmq")                                      # package_name

set(zmq_LIBS_RELEASE ${zmq_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${zmq_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND zmq_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND zmq_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${zmq_SYSTEM_LIBS_RELEASE})
    list(APPEND zmq_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND zmq_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(zmq_LIBRARIES_TARGETS_RELEASE "${zmq_LIBRARIES_TARGETS_RELEASE};libsodium::libsodium")
set(zmq_LIBRARIES_RELEASE "${zmq_LIBRARIES_RELEASE};libsodium::libsodium")

set(CMAKE_MODULE_PATH "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/Users/everbeek/.conan/data/zmq/4.2.5/bincrafters/stable/package/4a68df1aecde0f930b6e787577e197482049634b/" ${CMAKE_PREFIX_PATH})
