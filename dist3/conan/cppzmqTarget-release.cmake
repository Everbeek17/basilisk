
set(cppzmq_INCLUDE_DIRS_RELEASE "C:/Users/everbeek/.conan/data/cppzmq/4.3.0/bincrafters/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include")
set(cppzmq_INCLUDE_DIR_RELEASE "C:/Users/everbeek/.conan/data/cppzmq/4.3.0/bincrafters/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include")
set(cppzmq_INCLUDES_RELEASE "C:/Users/everbeek/.conan/data/cppzmq/4.3.0/bincrafters/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include")
set(cppzmq_RES_DIRS_RELEASE )
set(cppzmq_DEFINITIONS_RELEASE )
set(cppzmq_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(cppzmq_COMPILE_DEFINITIONS_RELEASE )
set(cppzmq_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(cppzmq_COMPILE_OPTIONS_C_RELEASE "")
set(cppzmq_COMPILE_OPTIONS_CXX_RELEASE "")
set(cppzmq_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(cppzmq_LIBRARIES_RELEASE "") # Will be filled later
set(cppzmq_LIBS_RELEASE "") # Same as cppzmq_LIBRARIES
set(cppzmq_SYSTEM_LIBS_RELEASE )
set(cppzmq_FRAMEWORK_DIRS_RELEASE )
set(cppzmq_FRAMEWORKS_RELEASE )
set(cppzmq_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(cppzmq_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(cppzmq_FRAMEWORKS_FOUND_RELEASE "${cppzmq_FRAMEWORKS_RELEASE}" "${cppzmq_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(cppzmq_INCLUDE_DIRS_RELEASE
                 cppzmq_INCLUDE_DIR_RELEASE
                 cppzmq_INCLUDES_RELEASE
                 cppzmq_DEFINITIONS_RELEASE
                 cppzmq_LINKER_FLAGS_RELEASE_LIST
                 cppzmq_COMPILE_DEFINITIONS_RELEASE
                 cppzmq_COMPILE_OPTIONS_RELEASE_LIST
                 cppzmq_LIBRARIES_RELEASE
                 cppzmq_LIBS_RELEASE
                 cppzmq_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to cppzmq_LIBS and cppzmq_LIBRARY_LIST
set(cppzmq_LIBRARY_LIST_RELEASE )
set(cppzmq_LIB_DIRS_RELEASE )

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_cppzmq_DEPENDENCIES_RELEASE "${cppzmq_FRAMEWORKS_FOUND_RELEASE} ${cppzmq_SYSTEM_LIBS_RELEASE} zmq::zmq")

conan_package_library_targets("${cppzmq_LIBRARY_LIST_RELEASE}"  # libraries
                              "${cppzmq_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_cppzmq_DEPENDENCIES_RELEASE}"  # deps
                              cppzmq_LIBRARIES_RELEASE            # out_libraries
                              cppzmq_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "cppzmq")                                      # package_name

set(cppzmq_LIBS_RELEASE ${cppzmq_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${cppzmq_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND cppzmq_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND cppzmq_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${cppzmq_SYSTEM_LIBS_RELEASE})
    list(APPEND cppzmq_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND cppzmq_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(cppzmq_LIBRARIES_TARGETS_RELEASE "${cppzmq_LIBRARIES_TARGETS_RELEASE};zmq::zmq")
set(cppzmq_LIBRARIES_RELEASE "${cppzmq_LIBRARIES_RELEASE};zmq::zmq")

set(CMAKE_MODULE_PATH "C:/Users/everbeek/.conan/data/cppzmq/4.3.0/bincrafters/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/Users/everbeek/.conan/data/cppzmq/4.3.0/bincrafters/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/" ${CMAKE_PREFIX_PATH})
