
if(NOT TARGET libsodium::libsodium)
    add_library(libsodium::libsodium INTERFACE IMPORTED)
endif()

# Load the debug and release library finders
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/libsodiumTarget-*.cmake")

foreach(f ${CONFIG_FILES})
    include(${f})
endforeach()
