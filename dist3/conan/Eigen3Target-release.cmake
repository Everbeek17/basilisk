
set(Eigen3_INCLUDE_DIRS_RELEASE "C:/Users/everbeek/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3")
set(Eigen3_INCLUDE_DIR_RELEASE "C:/Users/everbeek/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3")
set(Eigen3_INCLUDES_RELEASE "C:/Users/everbeek/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/include/eigen3")
set(Eigen3_RES_DIRS_RELEASE )
set(Eigen3_DEFINITIONS_RELEASE )
set(Eigen3_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(Eigen3_COMPILE_DEFINITIONS_RELEASE )
set(Eigen3_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(Eigen3_COMPILE_OPTIONS_C_RELEASE "")
set(Eigen3_COMPILE_OPTIONS_CXX_RELEASE "")
set(Eigen3_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(Eigen3_LIBRARIES_RELEASE "") # Will be filled later
set(Eigen3_LIBS_RELEASE "") # Same as Eigen3_LIBRARIES
set(Eigen3_SYSTEM_LIBS_RELEASE )
set(Eigen3_FRAMEWORK_DIRS_RELEASE )
set(Eigen3_FRAMEWORKS_RELEASE )
set(Eigen3_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(Eigen3_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(Eigen3_FRAMEWORKS_FOUND_RELEASE "${Eigen3_FRAMEWORKS_RELEASE}" "${Eigen3_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(Eigen3_INCLUDE_DIRS_RELEASE
                 Eigen3_INCLUDE_DIR_RELEASE
                 Eigen3_INCLUDES_RELEASE
                 Eigen3_DEFINITIONS_RELEASE
                 Eigen3_LINKER_FLAGS_RELEASE_LIST
                 Eigen3_COMPILE_DEFINITIONS_RELEASE
                 Eigen3_COMPILE_OPTIONS_RELEASE_LIST
                 Eigen3_LIBRARIES_RELEASE
                 Eigen3_LIBS_RELEASE
                 Eigen3_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to Eigen3_LIBS and Eigen3_LIBRARY_LIST
set(Eigen3_LIBRARY_LIST_RELEASE )
set(Eigen3_LIB_DIRS_RELEASE )

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_Eigen3_DEPENDENCIES_RELEASE "${Eigen3_FRAMEWORKS_FOUND_RELEASE} ${Eigen3_SYSTEM_LIBS_RELEASE} ")

conan_package_library_targets("${Eigen3_LIBRARY_LIST_RELEASE}"  # libraries
                              "${Eigen3_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_Eigen3_DEPENDENCIES_RELEASE}"  # deps
                              Eigen3_LIBRARIES_RELEASE            # out_libraries
                              Eigen3_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "Eigen3")                                      # package_name

set(Eigen3_LIBS_RELEASE ${Eigen3_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${Eigen3_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND Eigen3_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND Eigen3_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${Eigen3_SYSTEM_LIBS_RELEASE})
    list(APPEND Eigen3_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND Eigen3_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(Eigen3_LIBRARIES_TARGETS_RELEASE "${Eigen3_LIBRARIES_TARGETS_RELEASE};")
set(Eigen3_LIBRARIES_RELEASE "${Eigen3_LIBRARIES_RELEASE};")

set(CMAKE_MODULE_PATH "C:/Users/everbeek/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/Users/everbeek/.conan/data/eigen/3.3.7/conan/stable/package/5ab84d6acfe1f23c4fae0ab88f26e3a396351ac9/" ${CMAKE_PREFIX_PATH})
