
if(NOT TARGET protoc_installer::protoc_installer)
    add_library(protoc_installer::protoc_installer INTERFACE IMPORTED)
endif()

# Load the debug and release library finders
get_filename_component(_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
file(GLOB CONFIG_FILES "${_DIR}/protoc_installerTarget-*.cmake")

foreach(f ${CONFIG_FILES})
    include(${f})
endforeach()
