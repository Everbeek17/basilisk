
set(protobuf_INCLUDE_DIRS_RELEASE "C:/.conan/db84bf/1/include")
set(protobuf_INCLUDE_DIR_RELEASE "C:/.conan/db84bf/1/include")
set(protobuf_INCLUDES_RELEASE "C:/.conan/db84bf/1/include")
set(protobuf_RES_DIRS_RELEASE )
set(protobuf_DEFINITIONS_RELEASE "-DPROTOBUF_USE_DLLS")
set(protobuf_LINKER_FLAGS_RELEASE_LIST
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,SHARED_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,MODULE_LIBRARY>:>"
        "$<$<STREQUAL:$<TARGET_PROPERTY:TYPE>,EXECUTABLE>:>"
)
set(protobuf_COMPILE_DEFINITIONS_RELEASE "PROTOBUF_USE_DLLS")
set(protobuf_COMPILE_OPTIONS_RELEASE_LIST "" "")
set(protobuf_COMPILE_OPTIONS_C_RELEASE "")
set(protobuf_COMPILE_OPTIONS_CXX_RELEASE "")
set(protobuf_LIBRARIES_TARGETS_RELEASE "") # Will be filled later, if CMake 3
set(protobuf_LIBRARIES_RELEASE "") # Will be filled later
set(protobuf_LIBS_RELEASE "") # Same as protobuf_LIBRARIES
set(protobuf_SYSTEM_LIBS_RELEASE )
set(protobuf_FRAMEWORK_DIRS_RELEASE )
set(protobuf_FRAMEWORKS_RELEASE )
set(protobuf_FRAMEWORKS_FOUND_RELEASE "") # Will be filled later
set(protobuf_BUILD_MODULES_PATHS_RELEASE )

conan_find_apple_frameworks(protobuf_FRAMEWORKS_FOUND_RELEASE "${protobuf_FRAMEWORKS_RELEASE}" "${protobuf_FRAMEWORK_DIRS_RELEASE}")

mark_as_advanced(protobuf_INCLUDE_DIRS_RELEASE
                 protobuf_INCLUDE_DIR_RELEASE
                 protobuf_INCLUDES_RELEASE
                 protobuf_DEFINITIONS_RELEASE
                 protobuf_LINKER_FLAGS_RELEASE_LIST
                 protobuf_COMPILE_DEFINITIONS_RELEASE
                 protobuf_COMPILE_OPTIONS_RELEASE_LIST
                 protobuf_LIBRARIES_RELEASE
                 protobuf_LIBS_RELEASE
                 protobuf_LIBRARIES_TARGETS_RELEASE)

# Find the real .lib/.a and add them to protobuf_LIBS and protobuf_LIBRARY_LIST
set(protobuf_LIBRARY_LIST_RELEASE libprotobuf libprotoc)
set(protobuf_LIB_DIRS_RELEASE "C:/.conan/db84bf/1/lib")

# Gather all the libraries that should be linked to the targets (do not touch existing variables):
set(_protobuf_DEPENDENCIES_RELEASE "${protobuf_FRAMEWORKS_FOUND_RELEASE} ${protobuf_SYSTEM_LIBS_RELEASE} ")

conan_package_library_targets("${protobuf_LIBRARY_LIST_RELEASE}"  # libraries
                              "${protobuf_LIB_DIRS_RELEASE}"      # package_libdir
                              "${_protobuf_DEPENDENCIES_RELEASE}"  # deps
                              protobuf_LIBRARIES_RELEASE            # out_libraries
                              protobuf_LIBRARIES_TARGETS_RELEASE    # out_libraries_targets
                              "_RELEASE"                          # build_type
                              "protobuf")                                      # package_name

set(protobuf_LIBS_RELEASE ${protobuf_LIBRARIES_RELEASE})

foreach(_FRAMEWORK ${protobuf_FRAMEWORKS_FOUND_RELEASE})
    list(APPEND protobuf_LIBRARIES_TARGETS_RELEASE ${_FRAMEWORK})
    list(APPEND protobuf_LIBRARIES_RELEASE ${_FRAMEWORK})
endforeach()

foreach(_SYSTEM_LIB ${protobuf_SYSTEM_LIBS_RELEASE})
    list(APPEND protobuf_LIBRARIES_TARGETS_RELEASE ${_SYSTEM_LIB})
    list(APPEND protobuf_LIBRARIES_RELEASE ${_SYSTEM_LIB})
endforeach()

# We need to add our requirements too
set(protobuf_LIBRARIES_TARGETS_RELEASE "${protobuf_LIBRARIES_TARGETS_RELEASE};")
set(protobuf_LIBRARIES_RELEASE "${protobuf_LIBRARIES_RELEASE};")

set(CMAKE_MODULE_PATH "C:/.conan/db84bf/1/" ${CMAKE_MODULE_PATH})
set(CMAKE_PREFIX_PATH "C:/.conan/db84bf/1/" ${CMAKE_PREFIX_PATH})
