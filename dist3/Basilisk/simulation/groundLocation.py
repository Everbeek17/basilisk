# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _groundLocation
else:
    import _groundLocation

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *


def new_doubleArray(nelements):
    return _groundLocation.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _groundLocation.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _groundLocation.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _groundLocation.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _groundLocation.new_longArray(nelements)

def delete_longArray(ary):
    return _groundLocation.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _groundLocation.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _groundLocation.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _groundLocation.new_intArray(nelements)

def delete_intArray(ary):
    return _groundLocation.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _groundLocation.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _groundLocation.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _groundLocation.new_shortArray(nelements)

def delete_shortArray(ary):
    return _groundLocation.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _groundLocation.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _groundLocation.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _groundLocation.SysModel_swiginit(self, _groundLocation.new_SysModel(*args))
    __swig_destroy__ = _groundLocation.delete_SysModel

    def SelfInit(self):
        return _groundLocation.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _groundLocation.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _groundLocation.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _groundLocation.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_groundLocation.SysModel_ModelTag_get, _groundLocation.SysModel_ModelTag_set)
    CallCounts = property(_groundLocation.SysModel_CallCounts_get, _groundLocation.SysModel_CallCounts_set)
    RNGSeed = property(_groundLocation.SysModel_RNGSeed_get, _groundLocation.SysModel_RNGSeed_set)
    moduleID = property(_groundLocation.SysModel_moduleID_get, _groundLocation.SysModel_moduleID_set)

# Register SysModel in _groundLocation:
_groundLocation.SysModel_swigregister(SysModel)

class GroundLocation(SysModel):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _groundLocation.GroundLocation_swiginit(self, _groundLocation.new_GroundLocation())
    __swig_destroy__ = _groundLocation.delete_GroundLocation

    def UpdateState(self, CurrentSimNanos):
        return _groundLocation.GroundLocation_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _groundLocation.GroundLocation_Reset(self, CurrentSimNanos)

    def ReadMessages(self):
        return _groundLocation.GroundLocation_ReadMessages(self)

    def WriteMessages(self, CurrentClock):
        return _groundLocation.GroundLocation_WriteMessages(self, CurrentClock)

    def addSpacecraftToModel(self, tmpScMsg):
        return _groundLocation.GroundLocation_addSpacecraftToModel(self, tmpScMsg)

    def specifyLocation(self, lat, longitude, alt):
        return _groundLocation.GroundLocation_specifyLocation(self, lat, longitude, alt)
    planetRadius = property(_groundLocation.GroundLocation_planetRadius_get, _groundLocation.GroundLocation_planetRadius_set)
    minimumElevation = property(_groundLocation.GroundLocation_minimumElevation_get, _groundLocation.GroundLocation_minimumElevation_set)
    maximumRange = property(_groundLocation.GroundLocation_maximumRange_get, _groundLocation.GroundLocation_maximumRange_set)
    planetInMsg = property(_groundLocation.GroundLocation_planetInMsg_get, _groundLocation.GroundLocation_planetInMsg_set)
    currentGroundStateOutMsg = property(_groundLocation.GroundLocation_currentGroundStateOutMsg_get, _groundLocation.GroundLocation_currentGroundStateOutMsg_set)
    accessOutMsgs = property(_groundLocation.GroundLocation_accessOutMsgs_get, _groundLocation.GroundLocation_accessOutMsgs_set)
    scStateInMsgs = property(_groundLocation.GroundLocation_scStateInMsgs_get, _groundLocation.GroundLocation_scStateInMsgs_set)
    r_LP_P_Init = property(_groundLocation.GroundLocation_r_LP_P_Init_get, _groundLocation.GroundLocation_r_LP_P_Init_set)
    bskLogger = property(_groundLocation.GroundLocation_bskLogger_get, _groundLocation.GroundLocation_bskLogger_set)

# Register GroundLocation in _groundLocation:
_groundLocation.GroundLocation_swigregister(GroundLocation)

class SwigPyIterator(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _groundLocation.delete_SwigPyIterator

    def value(self):
        return _groundLocation.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _groundLocation.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _groundLocation.SwigPyIterator_decr(self, n)

    def distance(self, x):
        return _groundLocation.SwigPyIterator_distance(self, x)

    def equal(self, x):
        return _groundLocation.SwigPyIterator_equal(self, x)

    def copy(self):
        return _groundLocation.SwigPyIterator_copy(self)

    def next(self):
        return _groundLocation.SwigPyIterator_next(self)

    def __next__(self):
        return _groundLocation.SwigPyIterator___next__(self)

    def previous(self):
        return _groundLocation.SwigPyIterator_previous(self)

    def advance(self, n):
        return _groundLocation.SwigPyIterator_advance(self, n)

    def __eq__(self, x):
        return _groundLocation.SwigPyIterator___eq__(self, x)

    def __ne__(self, x):
        return _groundLocation.SwigPyIterator___ne__(self, x)

    def __iadd__(self, n):
        return _groundLocation.SwigPyIterator___iadd__(self, n)

    def __isub__(self, n):
        return _groundLocation.SwigPyIterator___isub__(self, n)

    def __add__(self, n):
        return _groundLocation.SwigPyIterator___add__(self, n)

    def __sub__(self, *args):
        return _groundLocation.SwigPyIterator___sub__(self, *args)
    def __iter__(self):
        return self

# Register SwigPyIterator in _groundLocation:
_groundLocation.SwigPyIterator_swigregister(SwigPyIterator)

MAX_BODY_NAME_LENGTH = _groundLocation.MAX_BODY_NAME_LENGTH
class SpicePlanetStateMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    J2000Current = property(_groundLocation.SpicePlanetStateMsgPayload_J2000Current_get, _groundLocation.SpicePlanetStateMsgPayload_J2000Current_set)
    PositionVector = property(_groundLocation.SpicePlanetStateMsgPayload_PositionVector_get, _groundLocation.SpicePlanetStateMsgPayload_PositionVector_set)
    VelocityVector = property(_groundLocation.SpicePlanetStateMsgPayload_VelocityVector_get, _groundLocation.SpicePlanetStateMsgPayload_VelocityVector_set)
    J20002Pfix = property(_groundLocation.SpicePlanetStateMsgPayload_J20002Pfix_get, _groundLocation.SpicePlanetStateMsgPayload_J20002Pfix_set)
    J20002Pfix_dot = property(_groundLocation.SpicePlanetStateMsgPayload_J20002Pfix_dot_get, _groundLocation.SpicePlanetStateMsgPayload_J20002Pfix_dot_set)
    computeOrient = property(_groundLocation.SpicePlanetStateMsgPayload_computeOrient_get, _groundLocation.SpicePlanetStateMsgPayload_computeOrient_set)
    PlanetName = property(_groundLocation.SpicePlanetStateMsgPayload_PlanetName_get, _groundLocation.SpicePlanetStateMsgPayload_PlanetName_set)

    def __init__(self):
        _groundLocation.SpicePlanetStateMsgPayload_swiginit(self, _groundLocation.new_SpicePlanetStateMsgPayload())
    __swig_destroy__ = _groundLocation.delete_SpicePlanetStateMsgPayload

# Register SpicePlanetStateMsgPayload in _groundLocation:
_groundLocation.SpicePlanetStateMsgPayload_swigregister(SpicePlanetStateMsgPayload)

class SCStatesMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    r_BN_N = property(_groundLocation.SCStatesMsgPayload_r_BN_N_get, _groundLocation.SCStatesMsgPayload_r_BN_N_set)
    v_BN_N = property(_groundLocation.SCStatesMsgPayload_v_BN_N_get, _groundLocation.SCStatesMsgPayload_v_BN_N_set)
    r_CN_N = property(_groundLocation.SCStatesMsgPayload_r_CN_N_get, _groundLocation.SCStatesMsgPayload_r_CN_N_set)
    v_CN_N = property(_groundLocation.SCStatesMsgPayload_v_CN_N_get, _groundLocation.SCStatesMsgPayload_v_CN_N_set)
    sigma_BN = property(_groundLocation.SCStatesMsgPayload_sigma_BN_get, _groundLocation.SCStatesMsgPayload_sigma_BN_set)
    omega_BN_B = property(_groundLocation.SCStatesMsgPayload_omega_BN_B_get, _groundLocation.SCStatesMsgPayload_omega_BN_B_set)
    omegaDot_BN_B = property(_groundLocation.SCStatesMsgPayload_omegaDot_BN_B_get, _groundLocation.SCStatesMsgPayload_omegaDot_BN_B_set)
    TotalAccumDVBdy = property(_groundLocation.SCStatesMsgPayload_TotalAccumDVBdy_get, _groundLocation.SCStatesMsgPayload_TotalAccumDVBdy_set)
    TotalAccumDV_BN_B = property(_groundLocation.SCStatesMsgPayload_TotalAccumDV_BN_B_get, _groundLocation.SCStatesMsgPayload_TotalAccumDV_BN_B_set)
    nonConservativeAccelpntB_B = property(_groundLocation.SCStatesMsgPayload_nonConservativeAccelpntB_B_get, _groundLocation.SCStatesMsgPayload_nonConservativeAccelpntB_B_set)
    MRPSwitchCount = property(_groundLocation.SCStatesMsgPayload_MRPSwitchCount_get, _groundLocation.SCStatesMsgPayload_MRPSwitchCount_set)

    def __init__(self):
        _groundLocation.SCStatesMsgPayload_swiginit(self, _groundLocation.new_SCStatesMsgPayload())
    __swig_destroy__ = _groundLocation.delete_SCStatesMsgPayload

# Register SCStatesMsgPayload in _groundLocation:
_groundLocation.SCStatesMsgPayload_swigregister(SCStatesMsgPayload)

class AccessMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    hasAccess = property(_groundLocation.AccessMsgPayload_hasAccess_get, _groundLocation.AccessMsgPayload_hasAccess_set)
    slantRange = property(_groundLocation.AccessMsgPayload_slantRange_get, _groundLocation.AccessMsgPayload_slantRange_set)
    elevation = property(_groundLocation.AccessMsgPayload_elevation_get, _groundLocation.AccessMsgPayload_elevation_set)
    azimuth = property(_groundLocation.AccessMsgPayload_azimuth_get, _groundLocation.AccessMsgPayload_azimuth_set)
    range_dot = property(_groundLocation.AccessMsgPayload_range_dot_get, _groundLocation.AccessMsgPayload_range_dot_set)
    el_dot = property(_groundLocation.AccessMsgPayload_el_dot_get, _groundLocation.AccessMsgPayload_el_dot_set)
    az_dot = property(_groundLocation.AccessMsgPayload_az_dot_get, _groundLocation.AccessMsgPayload_az_dot_set)
    r_BL_L = property(_groundLocation.AccessMsgPayload_r_BL_L_get, _groundLocation.AccessMsgPayload_r_BL_L_set)
    v_BL_L = property(_groundLocation.AccessMsgPayload_v_BL_L_get, _groundLocation.AccessMsgPayload_v_BL_L_set)

    def __init__(self):
        _groundLocation.AccessMsgPayload_swiginit(self, _groundLocation.new_AccessMsgPayload())
    __swig_destroy__ = _groundLocation.delete_AccessMsgPayload

# Register AccessMsgPayload in _groundLocation:
_groundLocation.AccessMsgPayload_swigregister(AccessMsgPayload)

class GroundStateMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    r_LN_N = property(_groundLocation.GroundStateMsgPayload_r_LN_N_get, _groundLocation.GroundStateMsgPayload_r_LN_N_set)
    r_LP_N = property(_groundLocation.GroundStateMsgPayload_r_LP_N_get, _groundLocation.GroundStateMsgPayload_r_LP_N_set)

    def __init__(self):
        _groundLocation.GroundStateMsgPayload_swiginit(self, _groundLocation.new_GroundStateMsgPayload())
    __swig_destroy__ = _groundLocation.delete_GroundStateMsgPayload

# Register GroundStateMsgPayload in _groundLocation:
_groundLocation.GroundStateMsgPayload_swigregister(GroundStateMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



