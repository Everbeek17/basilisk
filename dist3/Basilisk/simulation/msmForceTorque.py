# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _msmForceTorque
else:
    import _msmForceTorque

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *


def new_doubleArray(nelements):
    return _msmForceTorque.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _msmForceTorque.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _msmForceTorque.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _msmForceTorque.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _msmForceTorque.new_longArray(nelements)

def delete_longArray(ary):
    return _msmForceTorque.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _msmForceTorque.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _msmForceTorque.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _msmForceTorque.new_intArray(nelements)

def delete_intArray(ary):
    return _msmForceTorque.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _msmForceTorque.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _msmForceTorque.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _msmForceTorque.new_shortArray(nelements)

def delete_shortArray(ary):
    return _msmForceTorque.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _msmForceTorque.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _msmForceTorque.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


class SwigPyIterator(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _msmForceTorque.delete_SwigPyIterator

    def value(self):
        return _msmForceTorque.SwigPyIterator_value(self)

    def incr(self, n=1):
        return _msmForceTorque.SwigPyIterator_incr(self, n)

    def decr(self, n=1):
        return _msmForceTorque.SwigPyIterator_decr(self, n)

    def distance(self, x):
        return _msmForceTorque.SwigPyIterator_distance(self, x)

    def equal(self, x):
        return _msmForceTorque.SwigPyIterator_equal(self, x)

    def copy(self):
        return _msmForceTorque.SwigPyIterator_copy(self)

    def next(self):
        return _msmForceTorque.SwigPyIterator_next(self)

    def __next__(self):
        return _msmForceTorque.SwigPyIterator___next__(self)

    def previous(self):
        return _msmForceTorque.SwigPyIterator_previous(self)

    def advance(self, n):
        return _msmForceTorque.SwigPyIterator_advance(self, n)

    def __eq__(self, x):
        return _msmForceTorque.SwigPyIterator___eq__(self, x)

    def __ne__(self, x):
        return _msmForceTorque.SwigPyIterator___ne__(self, x)

    def __iadd__(self, n):
        return _msmForceTorque.SwigPyIterator___iadd__(self, n)

    def __isub__(self, n):
        return _msmForceTorque.SwigPyIterator___isub__(self, n)

    def __add__(self, n):
        return _msmForceTorque.SwigPyIterator___add__(self, n)

    def __sub__(self, *args):
        return _msmForceTorque.SwigPyIterator___sub__(self, *args)
    def __iter__(self):
        return self

# Register SwigPyIterator in _msmForceTorque:
_msmForceTorque.SwigPyIterator_swigregister(SwigPyIterator)

class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _msmForceTorque.SysModel_swiginit(self, _msmForceTorque.new_SysModel(*args))
    __swig_destroy__ = _msmForceTorque.delete_SysModel

    def SelfInit(self):
        return _msmForceTorque.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _msmForceTorque.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _msmForceTorque.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _msmForceTorque.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_msmForceTorque.SysModel_ModelTag_get, _msmForceTorque.SysModel_ModelTag_set)
    CallCounts = property(_msmForceTorque.SysModel_CallCounts_get, _msmForceTorque.SysModel_CallCounts_set)
    RNGSeed = property(_msmForceTorque.SysModel_RNGSeed_get, _msmForceTorque.SysModel_RNGSeed_set)
    moduleID = property(_msmForceTorque.SysModel_moduleID_get, _msmForceTorque.SysModel_moduleID_set)

# Register SysModel in _msmForceTorque:
_msmForceTorque.SysModel_swigregister(SysModel)

class MsmForceTorque(SysModel):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _msmForceTorque.MsmForceTorque_swiginit(self, _msmForceTorque.new_MsmForceTorque())
    __swig_destroy__ = _msmForceTorque.delete_MsmForceTorque

    def Reset(self, CurrentSimNanos):
        return _msmForceTorque.MsmForceTorque_Reset(self, CurrentSimNanos)

    def UpdateState(self, CurrentSimNanos):
        return _msmForceTorque.MsmForceTorque_UpdateState(self, CurrentSimNanos)

    def addSpacecraftToModel(self, tmpScMsg, radii, r_SB_B):
        return _msmForceTorque.MsmForceTorque_addSpacecraftToModel(self, tmpScMsg, radii, r_SB_B)
    scStateInMsgs = property(_msmForceTorque.MsmForceTorque_scStateInMsgs_get, _msmForceTorque.MsmForceTorque_scStateInMsgs_set)
    voltInMsgs = property(_msmForceTorque.MsmForceTorque_voltInMsgs_get, _msmForceTorque.MsmForceTorque_voltInMsgs_set)
    eTorqueOutMsgs = property(_msmForceTorque.MsmForceTorque_eTorqueOutMsgs_get, _msmForceTorque.MsmForceTorque_eTorqueOutMsgs_set)
    eForceOutMsgs = property(_msmForceTorque.MsmForceTorque_eForceOutMsgs_get, _msmForceTorque.MsmForceTorque_eForceOutMsgs_set)
    bskLogger = property(_msmForceTorque.MsmForceTorque_bskLogger_get, _msmForceTorque.MsmForceTorque_bskLogger_set)

# Register MsmForceTorque in _msmForceTorque:
_msmForceTorque.MsmForceTorque_swigregister(MsmForceTorque)

class SCStatesMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    r_BN_N = property(_msmForceTorque.SCStatesMsgPayload_r_BN_N_get, _msmForceTorque.SCStatesMsgPayload_r_BN_N_set)
    v_BN_N = property(_msmForceTorque.SCStatesMsgPayload_v_BN_N_get, _msmForceTorque.SCStatesMsgPayload_v_BN_N_set)
    r_CN_N = property(_msmForceTorque.SCStatesMsgPayload_r_CN_N_get, _msmForceTorque.SCStatesMsgPayload_r_CN_N_set)
    v_CN_N = property(_msmForceTorque.SCStatesMsgPayload_v_CN_N_get, _msmForceTorque.SCStatesMsgPayload_v_CN_N_set)
    sigma_BN = property(_msmForceTorque.SCStatesMsgPayload_sigma_BN_get, _msmForceTorque.SCStatesMsgPayload_sigma_BN_set)
    omega_BN_B = property(_msmForceTorque.SCStatesMsgPayload_omega_BN_B_get, _msmForceTorque.SCStatesMsgPayload_omega_BN_B_set)
    omegaDot_BN_B = property(_msmForceTorque.SCStatesMsgPayload_omegaDot_BN_B_get, _msmForceTorque.SCStatesMsgPayload_omegaDot_BN_B_set)
    TotalAccumDVBdy = property(_msmForceTorque.SCStatesMsgPayload_TotalAccumDVBdy_get, _msmForceTorque.SCStatesMsgPayload_TotalAccumDVBdy_set)
    TotalAccumDV_BN_B = property(_msmForceTorque.SCStatesMsgPayload_TotalAccumDV_BN_B_get, _msmForceTorque.SCStatesMsgPayload_TotalAccumDV_BN_B_set)
    nonConservativeAccelpntB_B = property(_msmForceTorque.SCStatesMsgPayload_nonConservativeAccelpntB_B_get, _msmForceTorque.SCStatesMsgPayload_nonConservativeAccelpntB_B_set)
    MRPSwitchCount = property(_msmForceTorque.SCStatesMsgPayload_MRPSwitchCount_get, _msmForceTorque.SCStatesMsgPayload_MRPSwitchCount_set)

    def __init__(self):
        _msmForceTorque.SCStatesMsgPayload_swiginit(self, _msmForceTorque.new_SCStatesMsgPayload())
    __swig_destroy__ = _msmForceTorque.delete_SCStatesMsgPayload

# Register SCStatesMsgPayload in _msmForceTorque:
_msmForceTorque.SCStatesMsgPayload_swigregister(SCStatesMsgPayload)

class VoltMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    voltage = property(_msmForceTorque.VoltMsgPayload_voltage_get, _msmForceTorque.VoltMsgPayload_voltage_set)

    def __init__(self):
        _msmForceTorque.VoltMsgPayload_swiginit(self, _msmForceTorque.new_VoltMsgPayload())
    __swig_destroy__ = _msmForceTorque.delete_VoltMsgPayload

# Register VoltMsgPayload in _msmForceTorque:
_msmForceTorque.VoltMsgPayload_swigregister(VoltMsgPayload)

class CmdTorqueBodyMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    torqueRequestBody = property(_msmForceTorque.CmdTorqueBodyMsgPayload_torqueRequestBody_get, _msmForceTorque.CmdTorqueBodyMsgPayload_torqueRequestBody_set)

    def __init__(self):
        _msmForceTorque.CmdTorqueBodyMsgPayload_swiginit(self, _msmForceTorque.new_CmdTorqueBodyMsgPayload())
    __swig_destroy__ = _msmForceTorque.delete_CmdTorqueBodyMsgPayload

# Register CmdTorqueBodyMsgPayload in _msmForceTorque:
_msmForceTorque.CmdTorqueBodyMsgPayload_swigregister(CmdTorqueBodyMsgPayload)

class CmdForceInertialMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    forceRequestInertial = property(_msmForceTorque.CmdForceInertialMsgPayload_forceRequestInertial_get, _msmForceTorque.CmdForceInertialMsgPayload_forceRequestInertial_set)

    def __init__(self):
        _msmForceTorque.CmdForceInertialMsgPayload_swiginit(self, _msmForceTorque.new_CmdForceInertialMsgPayload())
    __swig_destroy__ = _msmForceTorque.delete_CmdForceInertialMsgPayload

# Register CmdForceInertialMsgPayload in _msmForceTorque:
_msmForceTorque.CmdForceInertialMsgPayload_swigregister(CmdForceInertialMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



