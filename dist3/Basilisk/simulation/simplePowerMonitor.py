# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _simplePowerMonitor
else:
    import _simplePowerMonitor

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *

class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _simplePowerMonitor.SysModel_swiginit(self, _simplePowerMonitor.new_SysModel(*args))
    __swig_destroy__ = _simplePowerMonitor.delete_SysModel

    def SelfInit(self):
        return _simplePowerMonitor.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _simplePowerMonitor.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _simplePowerMonitor.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _simplePowerMonitor.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_simplePowerMonitor.SysModel_ModelTag_get, _simplePowerMonitor.SysModel_ModelTag_set)
    CallCounts = property(_simplePowerMonitor.SysModel_CallCounts_get, _simplePowerMonitor.SysModel_CallCounts_set)
    RNGSeed = property(_simplePowerMonitor.SysModel_RNGSeed_get, _simplePowerMonitor.SysModel_RNGSeed_set)
    moduleID = property(_simplePowerMonitor.SysModel_moduleID_get, _simplePowerMonitor.SysModel_moduleID_set)

# Register SysModel in _simplePowerMonitor:
_simplePowerMonitor.SysModel_swigregister(SysModel)

class PowerStorageBase(SysModel):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _simplePowerMonitor.delete_PowerStorageBase

    def Reset(self, CurrentSimNanos):
        return _simplePowerMonitor.PowerStorageBase_Reset(self, CurrentSimNanos)

    def addPowerNodeToModel(self, tmpNodeMsg):
        return _simplePowerMonitor.PowerStorageBase_addPowerNodeToModel(self, tmpNodeMsg)

    def UpdateState(self, CurrentSimNanos):
        return _simplePowerMonitor.PowerStorageBase_UpdateState(self, CurrentSimNanos)
    nodePowerUseInMsgs = property(_simplePowerMonitor.PowerStorageBase_nodePowerUseInMsgs_get, _simplePowerMonitor.PowerStorageBase_nodePowerUseInMsgs_set)
    batPowerOutMsg = property(_simplePowerMonitor.PowerStorageBase_batPowerOutMsg_get, _simplePowerMonitor.PowerStorageBase_batPowerOutMsg_set)
    storedCharge_Init = property(_simplePowerMonitor.PowerStorageBase_storedCharge_Init_get, _simplePowerMonitor.PowerStorageBase_storedCharge_Init_set)
    bskLogger = property(_simplePowerMonitor.PowerStorageBase_bskLogger_get, _simplePowerMonitor.PowerStorageBase_bskLogger_set)

# Register PowerStorageBase in _simplePowerMonitor:
_simplePowerMonitor.PowerStorageBase_swigregister(PowerStorageBase)

class SimplePowerMonitor(PowerStorageBase):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _simplePowerMonitor.SimplePowerMonitor_swiginit(self, _simplePowerMonitor.new_SimplePowerMonitor())
    __swig_destroy__ = _simplePowerMonitor.delete_SimplePowerMonitor

# Register SimplePowerMonitor in _simplePowerMonitor:
_simplePowerMonitor.SimplePowerMonitor_swigregister(SimplePowerMonitor)


def new_doubleArray(nelements):
    return _simplePowerMonitor.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _simplePowerMonitor.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _simplePowerMonitor.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _simplePowerMonitor.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _simplePowerMonitor.new_longArray(nelements)

def delete_longArray(ary):
    return _simplePowerMonitor.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _simplePowerMonitor.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _simplePowerMonitor.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _simplePowerMonitor.new_intArray(nelements)

def delete_intArray(ary):
    return _simplePowerMonitor.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _simplePowerMonitor.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _simplePowerMonitor.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _simplePowerMonitor.new_shortArray(nelements)

def delete_shortArray(ary):
    return _simplePowerMonitor.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _simplePowerMonitor.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _simplePowerMonitor.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


class PowerNodeUsageMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    netPower = property(_simplePowerMonitor.PowerNodeUsageMsgPayload_netPower_get, _simplePowerMonitor.PowerNodeUsageMsgPayload_netPower_set)

    def __init__(self):
        _simplePowerMonitor.PowerNodeUsageMsgPayload_swiginit(self, _simplePowerMonitor.new_PowerNodeUsageMsgPayload())
    __swig_destroy__ = _simplePowerMonitor.delete_PowerNodeUsageMsgPayload

# Register PowerNodeUsageMsgPayload in _simplePowerMonitor:
_simplePowerMonitor.PowerNodeUsageMsgPayload_swigregister(PowerNodeUsageMsgPayload)

class PowerStorageStatusMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    storageLevel = property(_simplePowerMonitor.PowerStorageStatusMsgPayload_storageLevel_get, _simplePowerMonitor.PowerStorageStatusMsgPayload_storageLevel_set)
    storageCapacity = property(_simplePowerMonitor.PowerStorageStatusMsgPayload_storageCapacity_get, _simplePowerMonitor.PowerStorageStatusMsgPayload_storageCapacity_set)
    currentNetPower = property(_simplePowerMonitor.PowerStorageStatusMsgPayload_currentNetPower_get, _simplePowerMonitor.PowerStorageStatusMsgPayload_currentNetPower_set)

    def __init__(self):
        _simplePowerMonitor.PowerStorageStatusMsgPayload_swiginit(self, _simplePowerMonitor.new_PowerStorageStatusMsgPayload())
    __swig_destroy__ = _simplePowerMonitor.delete_PowerStorageStatusMsgPayload

# Register PowerStorageStatusMsgPayload in _simplePowerMonitor:
_simplePowerMonitor.PowerStorageStatusMsgPayload_swigregister(PowerStorageStatusMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



