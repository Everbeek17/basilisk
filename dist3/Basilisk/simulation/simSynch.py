# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _simSynch
else:
    import _simSynch

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *

class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _simSynch.SysModel_swiginit(self, _simSynch.new_SysModel(*args))
    __swig_destroy__ = _simSynch.delete_SysModel

    def SelfInit(self):
        return _simSynch.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _simSynch.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _simSynch.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _simSynch.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_simSynch.SysModel_ModelTag_get, _simSynch.SysModel_ModelTag_set)
    CallCounts = property(_simSynch.SysModel_CallCounts_get, _simSynch.SysModel_CallCounts_set)
    RNGSeed = property(_simSynch.SysModel_RNGSeed_get, _simSynch.SysModel_RNGSeed_set)
    moduleID = property(_simSynch.SysModel_moduleID_get, _simSynch.SysModel_moduleID_set)

# Register SysModel in _simSynch:
_simSynch.SysModel_swigregister(SysModel)

class ClockSynch(SysModel):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _simSynch.ClockSynch_swiginit(self, _simSynch.new_ClockSynch())
    __swig_destroy__ = _simSynch.delete_ClockSynch

    def Reset(self, currentSimNanos):
        return _simSynch.ClockSynch_Reset(self, currentSimNanos)

    def UpdateState(self, currentSimNanos):
        return _simSynch.ClockSynch_UpdateState(self, currentSimNanos)
    accelFactor = property(_simSynch.ClockSynch_accelFactor_get, _simSynch.ClockSynch_accelFactor_set)
    outputData = property(_simSynch.ClockSynch_outputData_get, _simSynch.ClockSynch_outputData_set)
    clockOutMsg = property(_simSynch.ClockSynch_clockOutMsg_get, _simSynch.ClockSynch_clockOutMsg_set)
    accuracyNanos = property(_simSynch.ClockSynch_accuracyNanos_get, _simSynch.ClockSynch_accuracyNanos_set)
    displayTime = property(_simSynch.ClockSynch_displayTime_get, _simSynch.ClockSynch_displayTime_set)
    bskLogger = property(_simSynch.ClockSynch_bskLogger_get, _simSynch.ClockSynch_bskLogger_set)

# Register ClockSynch in _simSynch:
_simSynch.ClockSynch_swigregister(ClockSynch)

class SynchClockMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    initTimeDelta = property(_simSynch.SynchClockMsgPayload_initTimeDelta_get, _simSynch.SynchClockMsgPayload_initTimeDelta_set)
    finalTimeDelta = property(_simSynch.SynchClockMsgPayload_finalTimeDelta_get, _simSynch.SynchClockMsgPayload_finalTimeDelta_set)
    overrunCounter = property(_simSynch.SynchClockMsgPayload_overrunCounter_get, _simSynch.SynchClockMsgPayload_overrunCounter_set)

    def __init__(self):
        _simSynch.SynchClockMsgPayload_swiginit(self, _simSynch.new_SynchClockMsgPayload())
    __swig_destroy__ = _simSynch.delete_SynchClockMsgPayload

# Register SynchClockMsgPayload in _simSynch:
_simSynch.SynchClockMsgPayload_swigregister(SynchClockMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



