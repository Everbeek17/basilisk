# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _radiationPressure
else:
    import _radiationPressure

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *


def new_doubleArray(nelements):
    return _radiationPressure.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _radiationPressure.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _radiationPressure.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _radiationPressure.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _radiationPressure.new_longArray(nelements)

def delete_longArray(ary):
    return _radiationPressure.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _radiationPressure.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _radiationPressure.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _radiationPressure.new_intArray(nelements)

def delete_intArray(ary):
    return _radiationPressure.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _radiationPressure.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _radiationPressure.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _radiationPressure.new_shortArray(nelements)

def delete_shortArray(ary):
    return _radiationPressure.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _radiationPressure.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _radiationPressure.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


class DynamicEffector(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    __swig_destroy__ = _radiationPressure.delete_DynamicEffector

    def computeStateContribution(self, integTime):
        return _radiationPressure.DynamicEffector_computeStateContribution(self, integTime)

    def linkInStates(self, states):
        return _radiationPressure.DynamicEffector_linkInStates(self, states)

    def computeForceTorque(self, integTime):
        return _radiationPressure.DynamicEffector_computeForceTorque(self, integTime)
    stateDerivContribution = property(_radiationPressure.DynamicEffector_stateDerivContribution_get, _radiationPressure.DynamicEffector_stateDerivContribution_set)
    forceExternal_N = property(_radiationPressure.DynamicEffector_forceExternal_N_get, _radiationPressure.DynamicEffector_forceExternal_N_set)
    forceExternal_B = property(_radiationPressure.DynamicEffector_forceExternal_B_get, _radiationPressure.DynamicEffector_forceExternal_B_set)
    torqueExternalPntB_B = property(_radiationPressure.DynamicEffector_torqueExternalPntB_B_get, _radiationPressure.DynamicEffector_torqueExternalPntB_B_set)
    bskLogger = property(_radiationPressure.DynamicEffector_bskLogger_get, _radiationPressure.DynamicEffector_bskLogger_set)

# Register DynamicEffector in _radiationPressure:
_radiationPressure.DynamicEffector_swigregister(DynamicEffector)

class StateData(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    state = property(_radiationPressure.StateData_state_get, _radiationPressure.StateData_state_set)
    stateDeriv = property(_radiationPressure.StateData_stateDeriv_get, _radiationPressure.StateData_stateDeriv_set)
    stateName = property(_radiationPressure.StateData_stateName_get, _radiationPressure.StateData_stateName_set)
    stateEnabled = property(_radiationPressure.StateData_stateEnabled_get, _radiationPressure.StateData_stateEnabled_set)
    bskLogger = property(_radiationPressure.StateData_bskLogger_get, _radiationPressure.StateData_bskLogger_set)

    def __init__(self, *args):
        _radiationPressure.StateData_swiginit(self, _radiationPressure.new_StateData(*args))
    __swig_destroy__ = _radiationPressure.delete_StateData

    def setState(self, newState):
        return _radiationPressure.StateData_setState(self, newState)

    def propagateState(self, dt):
        return _radiationPressure.StateData_propagateState(self, dt)

    def setDerivative(self, newDeriv):
        return _radiationPressure.StateData_setDerivative(self, newDeriv)

    def getState(self):
        return _radiationPressure.StateData_getState(self)

    def getStateDeriv(self):
        return _radiationPressure.StateData_getStateDeriv(self)

    def getName(self):
        return _radiationPressure.StateData_getName(self)

    def getRowSize(self):
        return _radiationPressure.StateData_getRowSize(self)

    def getColumnSize(self):
        return _radiationPressure.StateData_getColumnSize(self)

    def isStateActive(self):
        return _radiationPressure.StateData_isStateActive(self)

    def disable(self):
        return _radiationPressure.StateData_disable(self)

    def enable(self):
        return _radiationPressure.StateData_enable(self)

    def scaleState(self, scaleFactor):
        return _radiationPressure.StateData_scaleState(self, scaleFactor)

    def __add__(self, operand):
        return _radiationPressure.StateData___add__(self, operand)

    def __mul__(self, scaleFactor):
        return _radiationPressure.StateData___mul__(self, scaleFactor)

# Register StateData in _radiationPressure:
_radiationPressure.StateData_swigregister(StateData)

class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _radiationPressure.SysModel_swiginit(self, _radiationPressure.new_SysModel(*args))
    __swig_destroy__ = _radiationPressure.delete_SysModel

    def SelfInit(self):
        return _radiationPressure.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _radiationPressure.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _radiationPressure.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _radiationPressure.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_radiationPressure.SysModel_ModelTag_get, _radiationPressure.SysModel_ModelTag_set)
    CallCounts = property(_radiationPressure.SysModel_CallCounts_get, _radiationPressure.SysModel_CallCounts_set)
    RNGSeed = property(_radiationPressure.SysModel_RNGSeed_get, _radiationPressure.SysModel_RNGSeed_set)
    moduleID = property(_radiationPressure.SysModel_moduleID_get, _radiationPressure.SysModel_moduleID_set)

# Register SysModel in _radiationPressure:
_radiationPressure.SysModel_swigregister(SysModel)

SRP_CANNONBALL_MODEL = _radiationPressure.SRP_CANNONBALL_MODEL
SRP_FACETED_CPU_MODEL = _radiationPressure.SRP_FACETED_CPU_MODEL
class RadiationPressure(SysModel, DynamicEffector):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _radiationPressure.RadiationPressure_swiginit(self, _radiationPressure.new_RadiationPressure())
    __swig_destroy__ = _radiationPressure.delete_RadiationPressure

    def Reset(self, CurrentSimNanos):
        return _radiationPressure.RadiationPressure_Reset(self, CurrentSimNanos)

    def UpdateState(self, CurrentSimNanos):
        return _radiationPressure.RadiationPressure_UpdateState(self, CurrentSimNanos)

    def linkInStates(self, statesIn):
        return _radiationPressure.RadiationPressure_linkInStates(self, statesIn)

    def readInputMessages(self):
        return _radiationPressure.RadiationPressure_readInputMessages(self)

    def computeForceTorque(self, integTime):
        return _radiationPressure.RadiationPressure_computeForceTorque(self, integTime)

    def setUseCannonballModel(self):
        return _radiationPressure.RadiationPressure_setUseCannonballModel(self)

    def setUseFacetedCPUModel(self):
        return _radiationPressure.RadiationPressure_setUseFacetedCPUModel(self)

    def addForceLookupBEntry(self, vec):
        return _radiationPressure.RadiationPressure_addForceLookupBEntry(self, vec)

    def addTorqueLookupBEntry(self, vec):
        return _radiationPressure.RadiationPressure_addTorqueLookupBEntry(self, vec)

    def addSHatLookupBEntry(self, vec):
        return _radiationPressure.RadiationPressure_addSHatLookupBEntry(self, vec)
    area = property(_radiationPressure.RadiationPressure_area_get, _radiationPressure.RadiationPressure_area_set)
    coefficientReflection = property(_radiationPressure.RadiationPressure_coefficientReflection_get, _radiationPressure.RadiationPressure_coefficientReflection_set)
    sunEphmInMsg = property(_radiationPressure.RadiationPressure_sunEphmInMsg_get, _radiationPressure.RadiationPressure_sunEphmInMsg_set)
    sunEclipseInMsg = property(_radiationPressure.RadiationPressure_sunEclipseInMsg_get, _radiationPressure.RadiationPressure_sunEclipseInMsg_set)
    lookupForce_B = property(_radiationPressure.RadiationPressure_lookupForce_B_get, _radiationPressure.RadiationPressure_lookupForce_B_set)
    lookupTorque_B = property(_radiationPressure.RadiationPressure_lookupTorque_B_get, _radiationPressure.RadiationPressure_lookupTorque_B_set)
    lookupSHat_B = property(_radiationPressure.RadiationPressure_lookupSHat_B_get, _radiationPressure.RadiationPressure_lookupSHat_B_set)
    bskLogger = property(_radiationPressure.RadiationPressure_bskLogger_get, _radiationPressure.RadiationPressure_bskLogger_set)

# Register RadiationPressure in _radiationPressure:
_radiationPressure.RadiationPressure_swigregister(RadiationPressure)

MAX_BODY_NAME_LENGTH = _radiationPressure.MAX_BODY_NAME_LENGTH
class SpicePlanetStateMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    J2000Current = property(_radiationPressure.SpicePlanetStateMsgPayload_J2000Current_get, _radiationPressure.SpicePlanetStateMsgPayload_J2000Current_set)
    PositionVector = property(_radiationPressure.SpicePlanetStateMsgPayload_PositionVector_get, _radiationPressure.SpicePlanetStateMsgPayload_PositionVector_set)
    VelocityVector = property(_radiationPressure.SpicePlanetStateMsgPayload_VelocityVector_get, _radiationPressure.SpicePlanetStateMsgPayload_VelocityVector_set)
    J20002Pfix = property(_radiationPressure.SpicePlanetStateMsgPayload_J20002Pfix_get, _radiationPressure.SpicePlanetStateMsgPayload_J20002Pfix_set)
    J20002Pfix_dot = property(_radiationPressure.SpicePlanetStateMsgPayload_J20002Pfix_dot_get, _radiationPressure.SpicePlanetStateMsgPayload_J20002Pfix_dot_set)
    computeOrient = property(_radiationPressure.SpicePlanetStateMsgPayload_computeOrient_get, _radiationPressure.SpicePlanetStateMsgPayload_computeOrient_set)
    PlanetName = property(_radiationPressure.SpicePlanetStateMsgPayload_PlanetName_get, _radiationPressure.SpicePlanetStateMsgPayload_PlanetName_set)

    def __init__(self):
        _radiationPressure.SpicePlanetStateMsgPayload_swiginit(self, _radiationPressure.new_SpicePlanetStateMsgPayload())
    __swig_destroy__ = _radiationPressure.delete_SpicePlanetStateMsgPayload

# Register SpicePlanetStateMsgPayload in _radiationPressure:
_radiationPressure.SpicePlanetStateMsgPayload_swigregister(SpicePlanetStateMsgPayload)

class EclipseMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    shadowFactor = property(_radiationPressure.EclipseMsgPayload_shadowFactor_get, _radiationPressure.EclipseMsgPayload_shadowFactor_set)

    def __init__(self):
        _radiationPressure.EclipseMsgPayload_swiginit(self, _radiationPressure.new_EclipseMsgPayload())
    __swig_destroy__ = _radiationPressure.delete_EclipseMsgPayload

# Register EclipseMsgPayload in _radiationPressure:
_radiationPressure.EclipseMsgPayload_swigregister(EclipseMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])

#
#  ISC License
#
#  Copyright (c) 2016, Autonomous Vehicle Systems Lab, University of Colorado at Boulder
#
#  Permission to use, copy, modify, and/or distribute this software for any
#  purpose with or without fee is hereby granted, provided that the above
#  copyright notice and this permission notice appear in all copies.
#
#  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
#  WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
#  MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
#  ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
#  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
#  ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
#  OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
#

from xml.etree import ElementTree
import numpy as np


class SRPLookupTableHandler:
    """Class to handle an SRP Lookup table"""
    def __init__(self):
        self.sHatBLookup = np.zeros([1, 3])
        self.forceBLookup = np.zeros([1, 3])
        self.torqueBLookup = np.zeros([1, 3])

    def parseAndLoadXML(self, filePath):
        document = ElementTree.parse(filePath)

        sHatBTree = document.find('sHatBValues')
        forceBTree = document.find('forceBValues')
        torqueBTree = document.find('torqueBValues')

        self.sHatBLookup.resize([len(list(sHatBTree)), 3])
        self.forceBLookup.resize([len(list(forceBTree)), 3])
        self.torqueBLookup.resize([len(list(torqueBTree)), 3])

        for node in list(sHatBTree):
            idx = int(node.attrib['index'])
            for value in list(node):
                if value.tag == 'value_1':
                    self.sHatBLookup[idx, 0] = value.text
                if value.tag == 'value_2':
                    self.sHatBLookup[idx, 1] = value.text
                if value.tag == 'value_3':
                    self.sHatBLookup[idx, 2] = value.text

        for node in list(forceBTree):
            idx = int(node.attrib['index'])
            for value in list(node):
                if value.tag == 'value_1':
                    self.forceBLookup[idx, 0] = value.text
                if value.tag == 'value_2':
                    self.forceBLookup[idx, 1] = value.text
                if value.tag == 'value_3':
                    self.forceBLookup[idx, 2] = value.text

        for node in list(torqueBTree):
            idx = int(node.attrib['index'])
            for value in list(node):
                if value.tag == 'value_1':
                    self.torqueBLookup[idx, 0] = value.text
                if value.tag == 'value_2':
                    self.torqueBLookup[idx, 1] = value.text
                if value.tag == 'value_3':
                    self.torqueBLookup[idx, 2] = value.text



