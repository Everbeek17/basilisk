# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _sphericalPendulum
else:
    import _sphericalPendulum

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



from Basilisk.architecture.swig_common_model import *


def new_doubleArray(nelements):
    return _sphericalPendulum.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _sphericalPendulum.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _sphericalPendulum.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _sphericalPendulum.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _sphericalPendulum.new_longArray(nelements)

def delete_longArray(ary):
    return _sphericalPendulum.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _sphericalPendulum.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _sphericalPendulum.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _sphericalPendulum.new_intArray(nelements)

def delete_intArray(ary):
    return _sphericalPendulum.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _sphericalPendulum.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _sphericalPendulum.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _sphericalPendulum.new_shortArray(nelements)

def delete_shortArray(ary):
    return _sphericalPendulum.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _sphericalPendulum.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _sphericalPendulum.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


class SysModel(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self, *args):
        _sphericalPendulum.SysModel_swiginit(self, _sphericalPendulum.new_SysModel(*args))
    __swig_destroy__ = _sphericalPendulum.delete_SysModel

    def SelfInit(self):
        return _sphericalPendulum.SysModel_SelfInit(self)

    def IntegratedInit(self):
        return _sphericalPendulum.SysModel_IntegratedInit(self)

    def UpdateState(self, CurrentSimNanos):
        return _sphericalPendulum.SysModel_UpdateState(self, CurrentSimNanos)

    def Reset(self, CurrentSimNanos):
        return _sphericalPendulum.SysModel_Reset(self, CurrentSimNanos)
    ModelTag = property(_sphericalPendulum.SysModel_ModelTag_get, _sphericalPendulum.SysModel_ModelTag_set)
    CallCounts = property(_sphericalPendulum.SysModel_CallCounts_get, _sphericalPendulum.SysModel_CallCounts_set)
    RNGSeed = property(_sphericalPendulum.SysModel_RNGSeed_get, _sphericalPendulum.SysModel_RNGSeed_set)
    moduleID = property(_sphericalPendulum.SysModel_moduleID_get, _sphericalPendulum.SysModel_moduleID_set)

# Register SysModel in _sphericalPendulum:
_sphericalPendulum.SysModel_swigregister(SysModel)

class FuelSlosh(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr

    def __init__(self):
        _sphericalPendulum.FuelSlosh_swiginit(self, _sphericalPendulum.new_FuelSlosh())
    __swig_destroy__ = _sphericalPendulum.delete_FuelSlosh

    def retrieveMassValue(self, integTime):
        return _sphericalPendulum.FuelSlosh_retrieveMassValue(self, integTime)
    fuelMass = property(_sphericalPendulum.FuelSlosh_fuelMass_get, _sphericalPendulum.FuelSlosh_fuelMass_set)
    massToTotalTankMassRatio = property(_sphericalPendulum.FuelSlosh_massToTotalTankMassRatio_get, _sphericalPendulum.FuelSlosh_massToTotalTankMassRatio_set)
    fuelMassDot = property(_sphericalPendulum.FuelSlosh_fuelMassDot_get, _sphericalPendulum.FuelSlosh_fuelMassDot_set)

# Register FuelSlosh in _sphericalPendulum:
_sphericalPendulum.FuelSlosh_swigregister(FuelSlosh)

class StateData(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    state = property(_sphericalPendulum.StateData_state_get, _sphericalPendulum.StateData_state_set)
    stateDeriv = property(_sphericalPendulum.StateData_stateDeriv_get, _sphericalPendulum.StateData_stateDeriv_set)
    stateName = property(_sphericalPendulum.StateData_stateName_get, _sphericalPendulum.StateData_stateName_set)
    stateEnabled = property(_sphericalPendulum.StateData_stateEnabled_get, _sphericalPendulum.StateData_stateEnabled_set)
    bskLogger = property(_sphericalPendulum.StateData_bskLogger_get, _sphericalPendulum.StateData_bskLogger_set)

    def __init__(self, *args):
        _sphericalPendulum.StateData_swiginit(self, _sphericalPendulum.new_StateData(*args))
    __swig_destroy__ = _sphericalPendulum.delete_StateData

    def setState(self, newState):
        return _sphericalPendulum.StateData_setState(self, newState)

    def propagateState(self, dt):
        return _sphericalPendulum.StateData_propagateState(self, dt)

    def setDerivative(self, newDeriv):
        return _sphericalPendulum.StateData_setDerivative(self, newDeriv)

    def getState(self):
        return _sphericalPendulum.StateData_getState(self)

    def getStateDeriv(self):
        return _sphericalPendulum.StateData_getStateDeriv(self)

    def getName(self):
        return _sphericalPendulum.StateData_getName(self)

    def getRowSize(self):
        return _sphericalPendulum.StateData_getRowSize(self)

    def getColumnSize(self):
        return _sphericalPendulum.StateData_getColumnSize(self)

    def isStateActive(self):
        return _sphericalPendulum.StateData_isStateActive(self)

    def disable(self):
        return _sphericalPendulum.StateData_disable(self)

    def enable(self):
        return _sphericalPendulum.StateData_enable(self)

    def scaleState(self, scaleFactor):
        return _sphericalPendulum.StateData_scaleState(self, scaleFactor)

    def __add__(self, operand):
        return _sphericalPendulum.StateData___add__(self, operand)

    def __mul__(self, scaleFactor):
        return _sphericalPendulum.StateData___mul__(self, scaleFactor)

# Register StateData in _sphericalPendulum:
_sphericalPendulum.StateData_swigregister(StateData)

class BackSubMatrices(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    matrixA = property(_sphericalPendulum.BackSubMatrices_matrixA_get, _sphericalPendulum.BackSubMatrices_matrixA_set)
    matrixB = property(_sphericalPendulum.BackSubMatrices_matrixB_get, _sphericalPendulum.BackSubMatrices_matrixB_set)
    matrixC = property(_sphericalPendulum.BackSubMatrices_matrixC_get, _sphericalPendulum.BackSubMatrices_matrixC_set)
    matrixD = property(_sphericalPendulum.BackSubMatrices_matrixD_get, _sphericalPendulum.BackSubMatrices_matrixD_set)
    vecTrans = property(_sphericalPendulum.BackSubMatrices_vecTrans_get, _sphericalPendulum.BackSubMatrices_vecTrans_set)
    vecRot = property(_sphericalPendulum.BackSubMatrices_vecRot_get, _sphericalPendulum.BackSubMatrices_vecRot_set)

    def __init__(self):
        _sphericalPendulum.BackSubMatrices_swiginit(self, _sphericalPendulum.new_BackSubMatrices())
    __swig_destroy__ = _sphericalPendulum.delete_BackSubMatrices

# Register BackSubMatrices in _sphericalPendulum:
_sphericalPendulum.BackSubMatrices_swigregister(BackSubMatrices)

class EffectorMassProps(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    mEff = property(_sphericalPendulum.EffectorMassProps_mEff_get, _sphericalPendulum.EffectorMassProps_mEff_set)
    mEffDot = property(_sphericalPendulum.EffectorMassProps_mEffDot_get, _sphericalPendulum.EffectorMassProps_mEffDot_set)
    IEffPntB_B = property(_sphericalPendulum.EffectorMassProps_IEffPntB_B_get, _sphericalPendulum.EffectorMassProps_IEffPntB_B_set)
    rEff_CB_B = property(_sphericalPendulum.EffectorMassProps_rEff_CB_B_get, _sphericalPendulum.EffectorMassProps_rEff_CB_B_set)
    rEffPrime_CB_B = property(_sphericalPendulum.EffectorMassProps_rEffPrime_CB_B_get, _sphericalPendulum.EffectorMassProps_rEffPrime_CB_B_set)
    IEffPrimePntB_B = property(_sphericalPendulum.EffectorMassProps_IEffPrimePntB_B_get, _sphericalPendulum.EffectorMassProps_IEffPrimePntB_B_set)

    def __init__(self):
        _sphericalPendulum.EffectorMassProps_swiginit(self, _sphericalPendulum.new_EffectorMassProps())
    __swig_destroy__ = _sphericalPendulum.delete_EffectorMassProps

# Register EffectorMassProps in _sphericalPendulum:
_sphericalPendulum.EffectorMassProps_swigregister(EffectorMassProps)

class StateEffector(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")

    def __init__(self, *args, **kwargs):
        raise AttributeError("No constructor defined - class is abstract")
    __repr__ = _swig_repr
    nameOfSpacecraftAttachedTo = property(_sphericalPendulum.StateEffector_nameOfSpacecraftAttachedTo_get, _sphericalPendulum.StateEffector_nameOfSpacecraftAttachedTo_set)
    effProps = property(_sphericalPendulum.StateEffector_effProps_get, _sphericalPendulum.StateEffector_effProps_set)
    forceOnBody_B = property(_sphericalPendulum.StateEffector_forceOnBody_B_get, _sphericalPendulum.StateEffector_forceOnBody_B_set)
    torqueOnBodyPntB_B = property(_sphericalPendulum.StateEffector_torqueOnBodyPntB_B_get, _sphericalPendulum.StateEffector_torqueOnBodyPntB_B_set)
    torqueOnBodyPntC_B = property(_sphericalPendulum.StateEffector_torqueOnBodyPntC_B_get, _sphericalPendulum.StateEffector_torqueOnBodyPntC_B_set)
    r_BP_P = property(_sphericalPendulum.StateEffector_r_BP_P_get, _sphericalPendulum.StateEffector_r_BP_P_set)
    dcm_BP = property(_sphericalPendulum.StateEffector_dcm_BP_get, _sphericalPendulum.StateEffector_dcm_BP_set)
    bskLogger = property(_sphericalPendulum.StateEffector_bskLogger_get, _sphericalPendulum.StateEffector_bskLogger_set)
    __swig_destroy__ = _sphericalPendulum.delete_StateEffector

    def updateEffectorMassProps(self, integTime):
        return _sphericalPendulum.StateEffector_updateEffectorMassProps(self, integTime)

    def updateContributions(self, integTime, backSubContr, sigma_BN, omega_BN_B, g_N):
        return _sphericalPendulum.StateEffector_updateContributions(self, integTime, backSubContr, sigma_BN, omega_BN_B, g_N)

    def updateEnergyMomContributions(self, integTime, rotAngMomPntCContr_B, rotEnergyContr, omega_BN_B):
        return _sphericalPendulum.StateEffector_updateEnergyMomContributions(self, integTime, rotAngMomPntCContr_B, rotEnergyContr, omega_BN_B)

    def modifyStates(self, integTime):
        return _sphericalPendulum.StateEffector_modifyStates(self, integTime)

    def calcForceTorqueOnBody(self, integTime, omega_BN_B):
        return _sphericalPendulum.StateEffector_calcForceTorqueOnBody(self, integTime, omega_BN_B)

    def writeOutputStateMessages(self, integTimeNanos):
        return _sphericalPendulum.StateEffector_writeOutputStateMessages(self, integTimeNanos)

    def registerStates(self, states):
        return _sphericalPendulum.StateEffector_registerStates(self, states)

    def linkInStates(self, states):
        return _sphericalPendulum.StateEffector_linkInStates(self, states)

    def computeDerivatives(self, integTime, rDDot_BN_N, omegaDot_BN_B, sigma_BN):
        return _sphericalPendulum.StateEffector_computeDerivatives(self, integTime, rDDot_BN_N, omegaDot_BN_B, sigma_BN)

    def prependSpacecraftNameToStates(self):
        return _sphericalPendulum.StateEffector_prependSpacecraftNameToStates(self)

    def receiveMotherSpacecraftData(self, rSC_BP_P, dcmSC_BP):
        return _sphericalPendulum.StateEffector_receiveMotherSpacecraftData(self, rSC_BP_P, dcmSC_BP)

# Register StateEffector in _sphericalPendulum:
_sphericalPendulum.StateEffector_swigregister(StateEffector)

class StateVector(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    stateMap = property(_sphericalPendulum.StateVector_stateMap_get, _sphericalPendulum.StateVector_stateMap_set)

    def __add__(self, operand):
        return _sphericalPendulum.StateVector___add__(self, operand)

    def __mul__(self, scaleFactor):
        return _sphericalPendulum.StateVector___mul__(self, scaleFactor)

    def __init__(self):
        _sphericalPendulum.StateVector_swiginit(self, _sphericalPendulum.new_StateVector())
    __swig_destroy__ = _sphericalPendulum.delete_StateVector

# Register StateVector in _sphericalPendulum:
_sphericalPendulum.StateVector_swigregister(StateVector)

class DynParamManager(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    dynProperties = property(_sphericalPendulum.DynParamManager_dynProperties_get, _sphericalPendulum.DynParamManager_dynProperties_set)
    stateContainer = property(_sphericalPendulum.DynParamManager_stateContainer_get, _sphericalPendulum.DynParamManager_stateContainer_set)
    bskLogger = property(_sphericalPendulum.DynParamManager_bskLogger_get, _sphericalPendulum.DynParamManager_bskLogger_set)

    def __init__(self):
        _sphericalPendulum.DynParamManager_swiginit(self, _sphericalPendulum.new_DynParamManager())
    __swig_destroy__ = _sphericalPendulum.delete_DynParamManager

    def registerState(self, nRow, nCol, stateName):
        return _sphericalPendulum.DynParamManager_registerState(self, nRow, nCol, stateName)

    def getStateObject(self, stateName):
        return _sphericalPendulum.DynParamManager_getStateObject(self, stateName)

    def getStateVector(self):
        return _sphericalPendulum.DynParamManager_getStateVector(self)

    def updateStateVector(self, newState):
        return _sphericalPendulum.DynParamManager_updateStateVector(self, newState)

    def propagateStateVector(self, dt):
        return _sphericalPendulum.DynParamManager_propagateStateVector(self, dt)

    def createProperty(self, propName, propValue):
        return _sphericalPendulum.DynParamManager_createProperty(self, propName, propValue)

    def getPropertyReference(self, propName):
        return _sphericalPendulum.DynParamManager_getPropertyReference(self, propName)

    def setPropertyValue(self, propName, propValue):
        return _sphericalPendulum.DynParamManager_setPropertyValue(self, propName, propValue)

# Register DynParamManager in _sphericalPendulum:
_sphericalPendulum.DynParamManager_swigregister(DynParamManager)

class SphericalPendulum(StateEffector, SysModel, FuelSlosh):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    pendulumRadius = property(_sphericalPendulum.SphericalPendulum_pendulumRadius_get, _sphericalPendulum.SphericalPendulum_pendulumRadius_set)
    D = property(_sphericalPendulum.SphericalPendulum_D_get, _sphericalPendulum.SphericalPendulum_D_set)
    phiDotInit = property(_sphericalPendulum.SphericalPendulum_phiDotInit_get, _sphericalPendulum.SphericalPendulum_phiDotInit_set)
    thetaDotInit = property(_sphericalPendulum.SphericalPendulum_thetaDotInit_get, _sphericalPendulum.SphericalPendulum_thetaDotInit_set)
    massInit = property(_sphericalPendulum.SphericalPendulum_massInit_get, _sphericalPendulum.SphericalPendulum_massInit_set)
    nameOfPhiState = property(_sphericalPendulum.SphericalPendulum_nameOfPhiState_get, _sphericalPendulum.SphericalPendulum_nameOfPhiState_set)
    nameOfThetaState = property(_sphericalPendulum.SphericalPendulum_nameOfThetaState_get, _sphericalPendulum.SphericalPendulum_nameOfThetaState_set)
    nameOfPhiDotState = property(_sphericalPendulum.SphericalPendulum_nameOfPhiDotState_get, _sphericalPendulum.SphericalPendulum_nameOfPhiDotState_set)
    nameOfThetaDotState = property(_sphericalPendulum.SphericalPendulum_nameOfThetaDotState_get, _sphericalPendulum.SphericalPendulum_nameOfThetaDotState_set)
    nameOfMassState = property(_sphericalPendulum.SphericalPendulum_nameOfMassState_get, _sphericalPendulum.SphericalPendulum_nameOfMassState_set)
    d = property(_sphericalPendulum.SphericalPendulum_d_get, _sphericalPendulum.SphericalPendulum_d_set)
    massState = property(_sphericalPendulum.SphericalPendulum_massState_get, _sphericalPendulum.SphericalPendulum_massState_set)
    pHat_01 = property(_sphericalPendulum.SphericalPendulum_pHat_01_get, _sphericalPendulum.SphericalPendulum_pHat_01_set)
    pHat_02 = property(_sphericalPendulum.SphericalPendulum_pHat_02_get, _sphericalPendulum.SphericalPendulum_pHat_02_set)
    pHat_03 = property(_sphericalPendulum.SphericalPendulum_pHat_03_get, _sphericalPendulum.SphericalPendulum_pHat_03_set)
    bskLogger = property(_sphericalPendulum.SphericalPendulum_bskLogger_get, _sphericalPendulum.SphericalPendulum_bskLogger_set)

    def __init__(self):
        _sphericalPendulum.SphericalPendulum_swiginit(self, _sphericalPendulum.new_SphericalPendulum())
    __swig_destroy__ = _sphericalPendulum.delete_SphericalPendulum

    def registerStates(self, states):
        return _sphericalPendulum.SphericalPendulum_registerStates(self, states)

    def linkInStates(self, states):
        return _sphericalPendulum.SphericalPendulum_linkInStates(self, states)

    def updateEffectorMassProps(self, integTime):
        return _sphericalPendulum.SphericalPendulum_updateEffectorMassProps(self, integTime)

    def modifyStates(self, integTime):
        return _sphericalPendulum.SphericalPendulum_modifyStates(self, integTime)

    def retrieveMassValue(self, integTime):
        return _sphericalPendulum.SphericalPendulum_retrieveMassValue(self, integTime)

    def updateContributions(self, integTime, backSubContr, sigma_BN, omega_BN_B, g_N):
        return _sphericalPendulum.SphericalPendulum_updateContributions(self, integTime, backSubContr, sigma_BN, omega_BN_B, g_N)

    def updateEnergyMomContributions(self, integTime, rotAngMomPntCContr_B, rotEnergyContr, omega_BN_B):
        return _sphericalPendulum.SphericalPendulum_updateEnergyMomContributions(self, integTime, rotAngMomPntCContr_B, rotEnergyContr, omega_BN_B)

    def computeDerivatives(self, integTime, rDDot_BN_N, omegaDot_BN_B, sigma_BN):
        return _sphericalPendulum.SphericalPendulum_computeDerivatives(self, integTime, rDDot_BN_N, omegaDot_BN_B, sigma_BN)

# Register SphericalPendulum in _sphericalPendulum:
_sphericalPendulum.SphericalPendulum_swigregister(SphericalPendulum)


import sys
protectAllClasses(sys.modules[__name__])



