# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _navAggregate
else:
    import _navAggregate

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _navAggregate.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _navAggregate.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _navAggregate.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _navAggregate.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _navAggregate.new_longArray(nelements)

def delete_longArray(ary):
    return _navAggregate.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _navAggregate.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _navAggregate.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _navAggregate.new_intArray(nelements)

def delete_intArray(ary):
    return _navAggregate.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _navAggregate.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _navAggregate.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _navAggregate.new_shortArray(nelements)

def delete_shortArray(ary):
    return _navAggregate.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _navAggregate.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _navAggregate.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


Update_aggregateNav = _navAggregate.Update_aggregateNav
SelfInit_aggregateNav = _navAggregate.SelfInit_aggregateNav
Reset_aggregateNav = _navAggregate.Reset_aggregateNav
MAX_AGG_NAV_MSG = _navAggregate.MAX_AGG_NAV_MSG
class AggregateAttInput(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    navAttInMsg = property(_navAggregate.AggregateAttInput_navAttInMsg_get, _navAggregate.AggregateAttInput_navAttInMsg_set)
    msgStorage = property(_navAggregate.AggregateAttInput_msgStorage_get, _navAggregate.AggregateAttInput_msgStorage_set)

    def __init__(self):
        _navAggregate.AggregateAttInput_swiginit(self, _navAggregate.new_AggregateAttInput())
    __swig_destroy__ = _navAggregate.delete_AggregateAttInput

# Register AggregateAttInput in _navAggregate:
_navAggregate.AggregateAttInput_swigregister(AggregateAttInput)

class AggregateTransInput(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    navTransInMsg = property(_navAggregate.AggregateTransInput_navTransInMsg_get, _navAggregate.AggregateTransInput_navTransInMsg_set)
    msgStorage = property(_navAggregate.AggregateTransInput_msgStorage_get, _navAggregate.AggregateTransInput_msgStorage_set)

    def __init__(self):
        _navAggregate.AggregateTransInput_swiginit(self, _navAggregate.new_AggregateTransInput())
    __swig_destroy__ = _navAggregate.delete_AggregateTransInput

# Register AggregateTransInput in _navAggregate:
_navAggregate.AggregateTransInput_swigregister(AggregateTransInput)

class NavAggregateData(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    attMsgs = property(_navAggregate.NavAggregateData_attMsgs_get, _navAggregate.NavAggregateData_attMsgs_set)
    transMsgs = property(_navAggregate.NavAggregateData_transMsgs_get, _navAggregate.NavAggregateData_transMsgs_set)
    navAttOutMsg = property(_navAggregate.NavAggregateData_navAttOutMsg_get, _navAggregate.NavAggregateData_navAttOutMsg_set)
    navTransOutMsg = property(_navAggregate.NavAggregateData_navTransOutMsg_get, _navAggregate.NavAggregateData_navTransOutMsg_set)
    attTimeIdx = property(_navAggregate.NavAggregateData_attTimeIdx_get, _navAggregate.NavAggregateData_attTimeIdx_set)
    transTimeIdx = property(_navAggregate.NavAggregateData_transTimeIdx_get, _navAggregate.NavAggregateData_transTimeIdx_set)
    attIdx = property(_navAggregate.NavAggregateData_attIdx_get, _navAggregate.NavAggregateData_attIdx_set)
    rateIdx = property(_navAggregate.NavAggregateData_rateIdx_get, _navAggregate.NavAggregateData_rateIdx_set)
    posIdx = property(_navAggregate.NavAggregateData_posIdx_get, _navAggregate.NavAggregateData_posIdx_set)
    velIdx = property(_navAggregate.NavAggregateData_velIdx_get, _navAggregate.NavAggregateData_velIdx_set)
    dvIdx = property(_navAggregate.NavAggregateData_dvIdx_get, _navAggregate.NavAggregateData_dvIdx_set)
    sunIdx = property(_navAggregate.NavAggregateData_sunIdx_get, _navAggregate.NavAggregateData_sunIdx_set)
    attMsgCount = property(_navAggregate.NavAggregateData_attMsgCount_get, _navAggregate.NavAggregateData_attMsgCount_set)
    transMsgCount = property(_navAggregate.NavAggregateData_transMsgCount_get, _navAggregate.NavAggregateData_transMsgCount_set)
    bskLogger = property(_navAggregate.NavAggregateData_bskLogger_get, _navAggregate.NavAggregateData_bskLogger_set)

    def __init__(self):
        _navAggregate.NavAggregateData_swiginit(self, _navAggregate.new_NavAggregateData())
    __swig_destroy__ = _navAggregate.delete_NavAggregateData

# Register NavAggregateData in _navAggregate:
_navAggregate.NavAggregateData_swigregister(NavAggregateData)

class NavAttMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_navAggregate.NavAttMsgPayload_timeTag_get, _navAggregate.NavAttMsgPayload_timeTag_set)
    sigma_BN = property(_navAggregate.NavAttMsgPayload_sigma_BN_get, _navAggregate.NavAttMsgPayload_sigma_BN_set)
    omega_BN_B = property(_navAggregate.NavAttMsgPayload_omega_BN_B_get, _navAggregate.NavAttMsgPayload_omega_BN_B_set)
    vehSunPntBdy = property(_navAggregate.NavAttMsgPayload_vehSunPntBdy_get, _navAggregate.NavAttMsgPayload_vehSunPntBdy_set)

    def __init__(self):
        _navAggregate.NavAttMsgPayload_swiginit(self, _navAggregate.new_NavAttMsgPayload())
    __swig_destroy__ = _navAggregate.delete_NavAttMsgPayload

# Register NavAttMsgPayload in _navAggregate:
_navAggregate.NavAttMsgPayload_swigregister(NavAttMsgPayload)

class NavTransMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_navAggregate.NavTransMsgPayload_timeTag_get, _navAggregate.NavTransMsgPayload_timeTag_set)
    r_BN_N = property(_navAggregate.NavTransMsgPayload_r_BN_N_get, _navAggregate.NavTransMsgPayload_r_BN_N_set)
    v_BN_N = property(_navAggregate.NavTransMsgPayload_v_BN_N_get, _navAggregate.NavTransMsgPayload_v_BN_N_set)
    vehAccumDV = property(_navAggregate.NavTransMsgPayload_vehAccumDV_get, _navAggregate.NavTransMsgPayload_vehAccumDV_set)

    def __init__(self):
        _navAggregate.NavTransMsgPayload_swiginit(self, _navAggregate.new_NavTransMsgPayload())
    __swig_destroy__ = _navAggregate.delete_NavTransMsgPayload

# Register NavTransMsgPayload in _navAggregate:
_navAggregate.NavTransMsgPayload_swigregister(NavTransMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



