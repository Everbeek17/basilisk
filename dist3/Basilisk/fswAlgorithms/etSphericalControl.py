# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _etSphericalControl
else:
    import _etSphericalControl

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _etSphericalControl.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _etSphericalControl.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _etSphericalControl.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _etSphericalControl.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _etSphericalControl.new_longArray(nelements)

def delete_longArray(ary):
    return _etSphericalControl.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _etSphericalControl.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _etSphericalControl.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _etSphericalControl.new_intArray(nelements)

def delete_intArray(ary):
    return _etSphericalControl.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _etSphericalControl.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _etSphericalControl.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _etSphericalControl.new_shortArray(nelements)

def delete_shortArray(ary):
    return _etSphericalControl.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _etSphericalControl.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _etSphericalControl.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


Update_etSphericalControl = _etSphericalControl.Update_etSphericalControl
SelfInit_etSphericalControl = _etSphericalControl.SelfInit_etSphericalControl
Reset_etSphericalControl = _etSphericalControl.Reset_etSphericalControl
class etSphericalControlConfig(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    servicerTransInMsg = property(_etSphericalControl.etSphericalControlConfig_servicerTransInMsg_get, _etSphericalControl.etSphericalControlConfig_servicerTransInMsg_set)
    debrisTransInMsg = property(_etSphericalControl.etSphericalControlConfig_debrisTransInMsg_get, _etSphericalControl.etSphericalControlConfig_debrisTransInMsg_set)
    servicerAttInMsg = property(_etSphericalControl.etSphericalControlConfig_servicerAttInMsg_get, _etSphericalControl.etSphericalControlConfig_servicerAttInMsg_set)
    servicerVehicleConfigInMsg = property(_etSphericalControl.etSphericalControlConfig_servicerVehicleConfigInMsg_get, _etSphericalControl.etSphericalControlConfig_servicerVehicleConfigInMsg_set)
    debrisVehicleConfigInMsg = property(_etSphericalControl.etSphericalControlConfig_debrisVehicleConfigInMsg_get, _etSphericalControl.etSphericalControlConfig_debrisVehicleConfigInMsg_set)
    eForceInMsg = property(_etSphericalControl.etSphericalControlConfig_eForceInMsg_get, _etSphericalControl.etSphericalControlConfig_eForceInMsg_set)
    forceInertialOutMsg = property(_etSphericalControl.etSphericalControlConfig_forceInertialOutMsg_get, _etSphericalControl.etSphericalControlConfig_forceInertialOutMsg_set)
    forceBodyOutMsg = property(_etSphericalControl.etSphericalControlConfig_forceBodyOutMsg_get, _etSphericalControl.etSphericalControlConfig_forceBodyOutMsg_set)
    mu = property(_etSphericalControl.etSphericalControlConfig_mu_get, _etSphericalControl.etSphericalControlConfig_mu_set)
    L_r = property(_etSphericalControl.etSphericalControlConfig_L_r_get, _etSphericalControl.etSphericalControlConfig_L_r_set)
    theta_r = property(_etSphericalControl.etSphericalControlConfig_theta_r_get, _etSphericalControl.etSphericalControlConfig_theta_r_set)
    phi_r = property(_etSphericalControl.etSphericalControlConfig_phi_r_get, _etSphericalControl.etSphericalControlConfig_phi_r_set)
    K = property(_etSphericalControl.etSphericalControlConfig_K_get, _etSphericalControl.etSphericalControlConfig_K_set)
    P = property(_etSphericalControl.etSphericalControlConfig_P_get, _etSphericalControl.etSphericalControlConfig_P_set)
    bskLogger = property(_etSphericalControl.etSphericalControlConfig_bskLogger_get, _etSphericalControl.etSphericalControlConfig_bskLogger_set)

    def __init__(self):
        _etSphericalControl.etSphericalControlConfig_swiginit(self, _etSphericalControl.new_etSphericalControlConfig())
    __swig_destroy__ = _etSphericalControl.delete_etSphericalControlConfig

# Register etSphericalControlConfig in _etSphericalControl:
_etSphericalControl.etSphericalControlConfig_swigregister(etSphericalControlConfig)


def calc_RelativeMotionControl(configData, servicerTransInMsgBuffer, debrisTransInMsgBuffer, servicerAttInMsgBuffer, servicerVehicleConfigInMsgBuffer, debrisVehicleConfigInMsgBuffer, eForceInMsgBuffer, forceInertialOutMsgBuffer, forceBodyOutMsgBuffer):
    return _etSphericalControl.calc_RelativeMotionControl(configData, servicerTransInMsgBuffer, debrisTransInMsgBuffer, servicerAttInMsgBuffer, servicerVehicleConfigInMsgBuffer, debrisVehicleConfigInMsgBuffer, eForceInMsgBuffer, forceInertialOutMsgBuffer, forceBodyOutMsgBuffer)
class NavTransMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_etSphericalControl.NavTransMsgPayload_timeTag_get, _etSphericalControl.NavTransMsgPayload_timeTag_set)
    r_BN_N = property(_etSphericalControl.NavTransMsgPayload_r_BN_N_get, _etSphericalControl.NavTransMsgPayload_r_BN_N_set)
    v_BN_N = property(_etSphericalControl.NavTransMsgPayload_v_BN_N_get, _etSphericalControl.NavTransMsgPayload_v_BN_N_set)
    vehAccumDV = property(_etSphericalControl.NavTransMsgPayload_vehAccumDV_get, _etSphericalControl.NavTransMsgPayload_vehAccumDV_set)

    def __init__(self):
        _etSphericalControl.NavTransMsgPayload_swiginit(self, _etSphericalControl.new_NavTransMsgPayload())
    __swig_destroy__ = _etSphericalControl.delete_NavTransMsgPayload

# Register NavTransMsgPayload in _etSphericalControl:
_etSphericalControl.NavTransMsgPayload_swigregister(NavTransMsgPayload)

class NavAttMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_etSphericalControl.NavAttMsgPayload_timeTag_get, _etSphericalControl.NavAttMsgPayload_timeTag_set)
    sigma_BN = property(_etSphericalControl.NavAttMsgPayload_sigma_BN_get, _etSphericalControl.NavAttMsgPayload_sigma_BN_set)
    omega_BN_B = property(_etSphericalControl.NavAttMsgPayload_omega_BN_B_get, _etSphericalControl.NavAttMsgPayload_omega_BN_B_set)
    vehSunPntBdy = property(_etSphericalControl.NavAttMsgPayload_vehSunPntBdy_get, _etSphericalControl.NavAttMsgPayload_vehSunPntBdy_set)

    def __init__(self):
        _etSphericalControl.NavAttMsgPayload_swiginit(self, _etSphericalControl.new_NavAttMsgPayload())
    __swig_destroy__ = _etSphericalControl.delete_NavAttMsgPayload

# Register NavAttMsgPayload in _etSphericalControl:
_etSphericalControl.NavAttMsgPayload_swigregister(NavAttMsgPayload)

class VehicleConfigMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    ISCPntB_B = property(_etSphericalControl.VehicleConfigMsgPayload_ISCPntB_B_get, _etSphericalControl.VehicleConfigMsgPayload_ISCPntB_B_set)
    CoM_B = property(_etSphericalControl.VehicleConfigMsgPayload_CoM_B_get, _etSphericalControl.VehicleConfigMsgPayload_CoM_B_set)
    massSC = property(_etSphericalControl.VehicleConfigMsgPayload_massSC_get, _etSphericalControl.VehicleConfigMsgPayload_massSC_set)
    CurrentADCSState = property(_etSphericalControl.VehicleConfigMsgPayload_CurrentADCSState_get, _etSphericalControl.VehicleConfigMsgPayload_CurrentADCSState_set)

    def __init__(self):
        _etSphericalControl.VehicleConfigMsgPayload_swiginit(self, _etSphericalControl.new_VehicleConfigMsgPayload())
    __swig_destroy__ = _etSphericalControl.delete_VehicleConfigMsgPayload

# Register VehicleConfigMsgPayload in _etSphericalControl:
_etSphericalControl.VehicleConfigMsgPayload_swigregister(VehicleConfigMsgPayload)

class CmdForceInertialMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    forceRequestInertial = property(_etSphericalControl.CmdForceInertialMsgPayload_forceRequestInertial_get, _etSphericalControl.CmdForceInertialMsgPayload_forceRequestInertial_set)

    def __init__(self):
        _etSphericalControl.CmdForceInertialMsgPayload_swiginit(self, _etSphericalControl.new_CmdForceInertialMsgPayload())
    __swig_destroy__ = _etSphericalControl.delete_CmdForceInertialMsgPayload

# Register CmdForceInertialMsgPayload in _etSphericalControl:
_etSphericalControl.CmdForceInertialMsgPayload_swigregister(CmdForceInertialMsgPayload)

class CmdForceBodyMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    forceRequestBody = property(_etSphericalControl.CmdForceBodyMsgPayload_forceRequestBody_get, _etSphericalControl.CmdForceBodyMsgPayload_forceRequestBody_set)

    def __init__(self):
        _etSphericalControl.CmdForceBodyMsgPayload_swiginit(self, _etSphericalControl.new_CmdForceBodyMsgPayload())
    __swig_destroy__ = _etSphericalControl.delete_CmdForceBodyMsgPayload

# Register CmdForceBodyMsgPayload in _etSphericalControl:
_etSphericalControl.CmdForceBodyMsgPayload_swigregister(CmdForceBodyMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



