# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _thrustRWDesat
else:
    import _thrustRWDesat

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _thrustRWDesat.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _thrustRWDesat.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _thrustRWDesat.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _thrustRWDesat.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _thrustRWDesat.new_longArray(nelements)

def delete_longArray(ary):
    return _thrustRWDesat.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _thrustRWDesat.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _thrustRWDesat.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _thrustRWDesat.new_intArray(nelements)

def delete_intArray(ary):
    return _thrustRWDesat.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _thrustRWDesat.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _thrustRWDesat.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _thrustRWDesat.new_shortArray(nelements)

def delete_shortArray(ary):
    return _thrustRWDesat.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _thrustRWDesat.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _thrustRWDesat.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


Update_thrustRWDesat = _thrustRWDesat.Update_thrustRWDesat
SelfInit_thrustRWDesat = _thrustRWDesat.SelfInit_thrustRWDesat
Reset_thrustRWDesat = _thrustRWDesat.Reset_thrustRWDesat
class RWSpeedMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    wheelSpeeds = property(_thrustRWDesat.RWSpeedMsgPayload_wheelSpeeds_get, _thrustRWDesat.RWSpeedMsgPayload_wheelSpeeds_set)
    wheelThetas = property(_thrustRWDesat.RWSpeedMsgPayload_wheelThetas_get, _thrustRWDesat.RWSpeedMsgPayload_wheelThetas_set)

    def __init__(self):
        _thrustRWDesat.RWSpeedMsgPayload_swiginit(self, _thrustRWDesat.new_RWSpeedMsgPayload())
    __swig_destroy__ = _thrustRWDesat.delete_RWSpeedMsgPayload

# Register RWSpeedMsgPayload in _thrustRWDesat:
_thrustRWDesat.RWSpeedMsgPayload_swigregister(RWSpeedMsgPayload)

class RWConstellationMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    numRW = property(_thrustRWDesat.RWConstellationMsgPayload_numRW_get, _thrustRWDesat.RWConstellationMsgPayload_numRW_set)
    reactionWheels = property(_thrustRWDesat.RWConstellationMsgPayload_reactionWheels_get, _thrustRWDesat.RWConstellationMsgPayload_reactionWheels_set)

    def __init__(self):
        _thrustRWDesat.RWConstellationMsgPayload_swiginit(self, _thrustRWDesat.new_RWConstellationMsgPayload())
    __swig_destroy__ = _thrustRWDesat.delete_RWConstellationMsgPayload

# Register RWConstellationMsgPayload in _thrustRWDesat:
_thrustRWDesat.RWConstellationMsgPayload_swigregister(RWConstellationMsgPayload)

class THRArrayConfigMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    numThrusters = property(_thrustRWDesat.THRArrayConfigMsgPayload_numThrusters_get, _thrustRWDesat.THRArrayConfigMsgPayload_numThrusters_set)
    thrusters = property(_thrustRWDesat.THRArrayConfigMsgPayload_thrusters_get, _thrustRWDesat.THRArrayConfigMsgPayload_thrusters_set)

    def __init__(self):
        _thrustRWDesat.THRArrayConfigMsgPayload_swiginit(self, _thrustRWDesat.new_THRArrayConfigMsgPayload())
    __swig_destroy__ = _thrustRWDesat.delete_THRArrayConfigMsgPayload

# Register THRArrayConfigMsgPayload in _thrustRWDesat:
_thrustRWDesat.THRArrayConfigMsgPayload_swigregister(THRArrayConfigMsgPayload)

class VehicleConfigMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    ISCPntB_B = property(_thrustRWDesat.VehicleConfigMsgPayload_ISCPntB_B_get, _thrustRWDesat.VehicleConfigMsgPayload_ISCPntB_B_set)
    CoM_B = property(_thrustRWDesat.VehicleConfigMsgPayload_CoM_B_get, _thrustRWDesat.VehicleConfigMsgPayload_CoM_B_set)
    massSC = property(_thrustRWDesat.VehicleConfigMsgPayload_massSC_get, _thrustRWDesat.VehicleConfigMsgPayload_massSC_set)
    CurrentADCSState = property(_thrustRWDesat.VehicleConfigMsgPayload_CurrentADCSState_get, _thrustRWDesat.VehicleConfigMsgPayload_CurrentADCSState_set)

    def __init__(self):
        _thrustRWDesat.VehicleConfigMsgPayload_swiginit(self, _thrustRWDesat.new_VehicleConfigMsgPayload())
    __swig_destroy__ = _thrustRWDesat.delete_VehicleConfigMsgPayload

# Register VehicleConfigMsgPayload in _thrustRWDesat:
_thrustRWDesat.VehicleConfigMsgPayload_swigregister(VehicleConfigMsgPayload)

class THRArrayOnTimeCmdMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    OnTimeRequest = property(_thrustRWDesat.THRArrayOnTimeCmdMsgPayload_OnTimeRequest_get, _thrustRWDesat.THRArrayOnTimeCmdMsgPayload_OnTimeRequest_set)

    def __init__(self):
        _thrustRWDesat.THRArrayOnTimeCmdMsgPayload_swiginit(self, _thrustRWDesat.new_THRArrayOnTimeCmdMsgPayload())
    __swig_destroy__ = _thrustRWDesat.delete_THRArrayOnTimeCmdMsgPayload

# Register THRArrayOnTimeCmdMsgPayload in _thrustRWDesat:
_thrustRWDesat.THRArrayOnTimeCmdMsgPayload_swigregister(THRArrayOnTimeCmdMsgPayload)

class thrustRWDesatConfig(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    rwSpeedInMsg = property(_thrustRWDesat.thrustRWDesatConfig_rwSpeedInMsg_get, _thrustRWDesat.thrustRWDesatConfig_rwSpeedInMsg_set)
    rwConfigInMsg = property(_thrustRWDesat.thrustRWDesatConfig_rwConfigInMsg_get, _thrustRWDesat.thrustRWDesatConfig_rwConfigInMsg_set)
    thrConfigInMsg = property(_thrustRWDesat.thrustRWDesatConfig_thrConfigInMsg_get, _thrustRWDesat.thrustRWDesatConfig_thrConfigInMsg_set)
    vecConfigInMsg = property(_thrustRWDesat.thrustRWDesatConfig_vecConfigInMsg_get, _thrustRWDesat.thrustRWDesatConfig_vecConfigInMsg_set)
    thrCmdOutMsg = property(_thrustRWDesat.thrustRWDesatConfig_thrCmdOutMsg_get, _thrustRWDesat.thrustRWDesatConfig_thrCmdOutMsg_set)
    rwAlignMap = property(_thrustRWDesat.thrustRWDesatConfig_rwAlignMap_get, _thrustRWDesat.thrustRWDesatConfig_rwAlignMap_set)
    thrAlignMap = property(_thrustRWDesat.thrustRWDesatConfig_thrAlignMap_get, _thrustRWDesat.thrustRWDesatConfig_thrAlignMap_set)
    thrTorqueMap = property(_thrustRWDesat.thrustRWDesatConfig_thrTorqueMap_get, _thrustRWDesat.thrustRWDesatConfig_thrTorqueMap_set)
    maxFiring = property(_thrustRWDesat.thrustRWDesatConfig_maxFiring_get, _thrustRWDesat.thrustRWDesatConfig_maxFiring_set)
    thrFiringPeriod = property(_thrustRWDesat.thrustRWDesatConfig_thrFiringPeriod_get, _thrustRWDesat.thrustRWDesatConfig_thrFiringPeriod_set)
    numRWAs = property(_thrustRWDesat.thrustRWDesatConfig_numRWAs_get, _thrustRWDesat.thrustRWDesatConfig_numRWAs_set)
    numThrusters = property(_thrustRWDesat.thrustRWDesatConfig_numThrusters_get, _thrustRWDesat.thrustRWDesatConfig_numThrusters_set)
    accumulatedImp = property(_thrustRWDesat.thrustRWDesatConfig_accumulatedImp_get, _thrustRWDesat.thrustRWDesatConfig_accumulatedImp_set)
    currDMDir = property(_thrustRWDesat.thrustRWDesatConfig_currDMDir_get, _thrustRWDesat.thrustRWDesatConfig_currDMDir_set)
    totalAccumFiring = property(_thrustRWDesat.thrustRWDesatConfig_totalAccumFiring_get, _thrustRWDesat.thrustRWDesatConfig_totalAccumFiring_set)
    DMThresh = property(_thrustRWDesat.thrustRWDesatConfig_DMThresh_get, _thrustRWDesat.thrustRWDesatConfig_DMThresh_set)
    previousFiring = property(_thrustRWDesat.thrustRWDesatConfig_previousFiring_get, _thrustRWDesat.thrustRWDesatConfig_previousFiring_set)
    bskLogger = property(_thrustRWDesat.thrustRWDesatConfig_bskLogger_get, _thrustRWDesat.thrustRWDesatConfig_bskLogger_set)

    def __init__(self):
        _thrustRWDesat.thrustRWDesatConfig_swiginit(self, _thrustRWDesat.new_thrustRWDesatConfig())
    __swig_destroy__ = _thrustRWDesat.delete_thrustRWDesatConfig

# Register thrustRWDesatConfig in _thrustRWDesat:
_thrustRWDesat.thrustRWDesatConfig_swigregister(thrustRWDesatConfig)


import sys
protectAllClasses(sys.modules[__name__])



