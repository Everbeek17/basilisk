# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _hillPoint
else:
    import _hillPoint

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _hillPoint.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _hillPoint.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _hillPoint.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _hillPoint.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _hillPoint.new_longArray(nelements)

def delete_longArray(ary):
    return _hillPoint.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _hillPoint.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _hillPoint.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _hillPoint.new_intArray(nelements)

def delete_intArray(ary):
    return _hillPoint.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _hillPoint.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _hillPoint.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _hillPoint.new_shortArray(nelements)

def delete_shortArray(ary):
    return _hillPoint.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _hillPoint.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _hillPoint.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


Update_hillPoint = _hillPoint.Update_hillPoint
SelfInit_hillPoint = _hillPoint.SelfInit_hillPoint
Reset_hillPoint = _hillPoint.Reset_hillPoint
class hillPointConfig(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    attRefOutMsg = property(_hillPoint.hillPointConfig_attRefOutMsg_get, _hillPoint.hillPointConfig_attRefOutMsg_set)
    transNavInMsg = property(_hillPoint.hillPointConfig_transNavInMsg_get, _hillPoint.hillPointConfig_transNavInMsg_set)
    celBodyInMsg = property(_hillPoint.hillPointConfig_celBodyInMsg_get, _hillPoint.hillPointConfig_celBodyInMsg_set)
    planetMsgIsLinked = property(_hillPoint.hillPointConfig_planetMsgIsLinked_get, _hillPoint.hillPointConfig_planetMsgIsLinked_set)
    bskLogger = property(_hillPoint.hillPointConfig_bskLogger_get, _hillPoint.hillPointConfig_bskLogger_set)

    def __init__(self):
        _hillPoint.hillPointConfig_swiginit(self, _hillPoint.new_hillPointConfig())
    __swig_destroy__ = _hillPoint.delete_hillPointConfig

# Register hillPointConfig in _hillPoint:
_hillPoint.hillPointConfig_swigregister(hillPointConfig)


def computeHillPointingReference(configData, r_BN_N, v_BN_N, celBdyPositonVector, celBdyVelocityVector, attRefOut):
    return _hillPoint.computeHillPointingReference(configData, r_BN_N, v_BN_N, celBdyPositonVector, celBdyVelocityVector, attRefOut)
class EphemerisMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    r_BdyZero_N = property(_hillPoint.EphemerisMsgPayload_r_BdyZero_N_get, _hillPoint.EphemerisMsgPayload_r_BdyZero_N_set)
    v_BdyZero_N = property(_hillPoint.EphemerisMsgPayload_v_BdyZero_N_get, _hillPoint.EphemerisMsgPayload_v_BdyZero_N_set)
    sigma_BN = property(_hillPoint.EphemerisMsgPayload_sigma_BN_get, _hillPoint.EphemerisMsgPayload_sigma_BN_set)
    omega_BN_B = property(_hillPoint.EphemerisMsgPayload_omega_BN_B_get, _hillPoint.EphemerisMsgPayload_omega_BN_B_set)
    timeTag = property(_hillPoint.EphemerisMsgPayload_timeTag_get, _hillPoint.EphemerisMsgPayload_timeTag_set)

    def __init__(self):
        _hillPoint.EphemerisMsgPayload_swiginit(self, _hillPoint.new_EphemerisMsgPayload())
    __swig_destroy__ = _hillPoint.delete_EphemerisMsgPayload

# Register EphemerisMsgPayload in _hillPoint:
_hillPoint.EphemerisMsgPayload_swigregister(EphemerisMsgPayload)

class NavTransMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_hillPoint.NavTransMsgPayload_timeTag_get, _hillPoint.NavTransMsgPayload_timeTag_set)
    r_BN_N = property(_hillPoint.NavTransMsgPayload_r_BN_N_get, _hillPoint.NavTransMsgPayload_r_BN_N_set)
    v_BN_N = property(_hillPoint.NavTransMsgPayload_v_BN_N_get, _hillPoint.NavTransMsgPayload_v_BN_N_set)
    vehAccumDV = property(_hillPoint.NavTransMsgPayload_vehAccumDV_get, _hillPoint.NavTransMsgPayload_vehAccumDV_set)

    def __init__(self):
        _hillPoint.NavTransMsgPayload_swiginit(self, _hillPoint.new_NavTransMsgPayload())
    __swig_destroy__ = _hillPoint.delete_NavTransMsgPayload

# Register NavTransMsgPayload in _hillPoint:
_hillPoint.NavTransMsgPayload_swigregister(NavTransMsgPayload)

class AttRefMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    sigma_RN = property(_hillPoint.AttRefMsgPayload_sigma_RN_get, _hillPoint.AttRefMsgPayload_sigma_RN_set)
    omega_RN_N = property(_hillPoint.AttRefMsgPayload_omega_RN_N_get, _hillPoint.AttRefMsgPayload_omega_RN_N_set)
    domega_RN_N = property(_hillPoint.AttRefMsgPayload_domega_RN_N_get, _hillPoint.AttRefMsgPayload_domega_RN_N_set)

    def __init__(self):
        _hillPoint.AttRefMsgPayload_swiginit(self, _hillPoint.new_AttRefMsgPayload())
    __swig_destroy__ = _hillPoint.delete_AttRefMsgPayload

# Register AttRefMsgPayload in _hillPoint:
_hillPoint.AttRefMsgPayload_swigregister(AttRefMsgPayload)


import sys
protectAllClasses(sys.modules[__name__])



