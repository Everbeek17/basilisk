# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _horizonOpNav
else:
    import _horizonOpNav

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _horizonOpNav.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _horizonOpNav.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _horizonOpNav.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _horizonOpNav.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _horizonOpNav.new_longArray(nelements)

def delete_longArray(ary):
    return _horizonOpNav.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _horizonOpNav.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _horizonOpNav.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _horizonOpNav.new_intArray(nelements)

def delete_intArray(ary):
    return _horizonOpNav.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _horizonOpNav.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _horizonOpNav.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _horizonOpNav.new_shortArray(nelements)

def delete_shortArray(ary):
    return _horizonOpNav.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _horizonOpNav.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _horizonOpNav.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


SelfInit_horizonOpNav = _horizonOpNav.SelfInit_horizonOpNav
Reset_horizonOpNav = _horizonOpNav.Reset_horizonOpNav
Update_horizonOpNav = _horizonOpNav.Update_horizonOpNav
class OpNavLimbMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_horizonOpNav.OpNavLimbMsgPayload_timeTag_get, _horizonOpNav.OpNavLimbMsgPayload_timeTag_set)
    valid = property(_horizonOpNav.OpNavLimbMsgPayload_valid_get, _horizonOpNav.OpNavLimbMsgPayload_valid_set)
    numLimbPoints = property(_horizonOpNav.OpNavLimbMsgPayload_numLimbPoints_get, _horizonOpNav.OpNavLimbMsgPayload_numLimbPoints_set)
    cameraID = property(_horizonOpNav.OpNavLimbMsgPayload_cameraID_get, _horizonOpNav.OpNavLimbMsgPayload_cameraID_set)
    planetIds = property(_horizonOpNav.OpNavLimbMsgPayload_planetIds_get, _horizonOpNav.OpNavLimbMsgPayload_planetIds_set)
    limbPoints = property(_horizonOpNav.OpNavLimbMsgPayload_limbPoints_get, _horizonOpNav.OpNavLimbMsgPayload_limbPoints_set)

    def __init__(self):
        _horizonOpNav.OpNavLimbMsgPayload_swiginit(self, _horizonOpNav.new_OpNavLimbMsgPayload())
    __swig_destroy__ = _horizonOpNav.delete_OpNavLimbMsgPayload

# Register OpNavLimbMsgPayload in _horizonOpNav:
_horizonOpNav.OpNavLimbMsgPayload_swigregister(OpNavLimbMsgPayload)

MAX_STRING_LENGTH = _horizonOpNav.MAX_STRING_LENGTH
class CameraConfigMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    cameraID = property(_horizonOpNav.CameraConfigMsgPayload_cameraID_get, _horizonOpNav.CameraConfigMsgPayload_cameraID_set)
    isOn = property(_horizonOpNav.CameraConfigMsgPayload_isOn_get, _horizonOpNav.CameraConfigMsgPayload_isOn_set)
    parentName = property(_horizonOpNav.CameraConfigMsgPayload_parentName_get, _horizonOpNav.CameraConfigMsgPayload_parentName_set)
    fieldOfView = property(_horizonOpNav.CameraConfigMsgPayload_fieldOfView_get, _horizonOpNav.CameraConfigMsgPayload_fieldOfView_set)
    resolution = property(_horizonOpNav.CameraConfigMsgPayload_resolution_get, _horizonOpNav.CameraConfigMsgPayload_resolution_set)
    renderRate = property(_horizonOpNav.CameraConfigMsgPayload_renderRate_get, _horizonOpNav.CameraConfigMsgPayload_renderRate_set)
    cameraPos_B = property(_horizonOpNav.CameraConfigMsgPayload_cameraPos_B_get, _horizonOpNav.CameraConfigMsgPayload_cameraPos_B_set)
    sigma_CB = property(_horizonOpNav.CameraConfigMsgPayload_sigma_CB_get, _horizonOpNav.CameraConfigMsgPayload_sigma_CB_set)
    skyBox = property(_horizonOpNav.CameraConfigMsgPayload_skyBox_get, _horizonOpNav.CameraConfigMsgPayload_skyBox_set)

    def __init__(self):
        _horizonOpNav.CameraConfigMsgPayload_swiginit(self, _horizonOpNav.new_CameraConfigMsgPayload())
    __swig_destroy__ = _horizonOpNav.delete_CameraConfigMsgPayload

# Register CameraConfigMsgPayload in _horizonOpNav:
_horizonOpNav.CameraConfigMsgPayload_swigregister(CameraConfigMsgPayload)

class NavAttMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_horizonOpNav.NavAttMsgPayload_timeTag_get, _horizonOpNav.NavAttMsgPayload_timeTag_set)
    sigma_BN = property(_horizonOpNav.NavAttMsgPayload_sigma_BN_get, _horizonOpNav.NavAttMsgPayload_sigma_BN_set)
    omega_BN_B = property(_horizonOpNav.NavAttMsgPayload_omega_BN_B_get, _horizonOpNav.NavAttMsgPayload_omega_BN_B_set)
    vehSunPntBdy = property(_horizonOpNav.NavAttMsgPayload_vehSunPntBdy_get, _horizonOpNav.NavAttMsgPayload_vehSunPntBdy_set)

    def __init__(self):
        _horizonOpNav.NavAttMsgPayload_swiginit(self, _horizonOpNav.new_NavAttMsgPayload())
    __swig_destroy__ = _horizonOpNav.delete_NavAttMsgPayload

# Register NavAttMsgPayload in _horizonOpNav:
_horizonOpNav.NavAttMsgPayload_swigregister(NavAttMsgPayload)

class OpNavMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_horizonOpNav.OpNavMsgPayload_timeTag_get, _horizonOpNav.OpNavMsgPayload_timeTag_set)
    valid = property(_horizonOpNav.OpNavMsgPayload_valid_get, _horizonOpNav.OpNavMsgPayload_valid_set)
    covar_N = property(_horizonOpNav.OpNavMsgPayload_covar_N_get, _horizonOpNav.OpNavMsgPayload_covar_N_set)
    covar_B = property(_horizonOpNav.OpNavMsgPayload_covar_B_get, _horizonOpNav.OpNavMsgPayload_covar_B_set)
    covar_C = property(_horizonOpNav.OpNavMsgPayload_covar_C_get, _horizonOpNav.OpNavMsgPayload_covar_C_set)
    r_BN_N = property(_horizonOpNav.OpNavMsgPayload_r_BN_N_get, _horizonOpNav.OpNavMsgPayload_r_BN_N_set)
    r_BN_B = property(_horizonOpNav.OpNavMsgPayload_r_BN_B_get, _horizonOpNav.OpNavMsgPayload_r_BN_B_set)
    r_BN_C = property(_horizonOpNav.OpNavMsgPayload_r_BN_C_get, _horizonOpNav.OpNavMsgPayload_r_BN_C_set)
    planetID = property(_horizonOpNav.OpNavMsgPayload_planetID_get, _horizonOpNav.OpNavMsgPayload_planetID_set)
    faultDetected = property(_horizonOpNav.OpNavMsgPayload_faultDetected_get, _horizonOpNav.OpNavMsgPayload_faultDetected_set)

    def __init__(self):
        _horizonOpNav.OpNavMsgPayload_swiginit(self, _horizonOpNav.new_OpNavMsgPayload())
    __swig_destroy__ = _horizonOpNav.delete_OpNavMsgPayload

# Register OpNavMsgPayload in _horizonOpNav:
_horizonOpNav.OpNavMsgPayload_swigregister(OpNavMsgPayload)

class HorizonOpNavData(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    opNavOutMsg = property(_horizonOpNav.HorizonOpNavData_opNavOutMsg_get, _horizonOpNav.HorizonOpNavData_opNavOutMsg_set)
    cameraConfigInMsg = property(_horizonOpNav.HorizonOpNavData_cameraConfigInMsg_get, _horizonOpNav.HorizonOpNavData_cameraConfigInMsg_set)
    attInMsg = property(_horizonOpNav.HorizonOpNavData_attInMsg_get, _horizonOpNav.HorizonOpNavData_attInMsg_set)
    limbInMsg = property(_horizonOpNav.HorizonOpNavData_limbInMsg_get, _horizonOpNav.HorizonOpNavData_limbInMsg_set)
    planetTarget = property(_horizonOpNav.HorizonOpNavData_planetTarget_get, _horizonOpNav.HorizonOpNavData_planetTarget_set)
    noiseSF = property(_horizonOpNav.HorizonOpNavData_noiseSF_get, _horizonOpNav.HorizonOpNavData_noiseSF_set)
    bskLogger = property(_horizonOpNav.HorizonOpNavData_bskLogger_get, _horizonOpNav.HorizonOpNavData_bskLogger_set)

    def __init__(self):
        _horizonOpNav.HorizonOpNavData_swiginit(self, _horizonOpNav.new_HorizonOpNavData())
    __swig_destroy__ = _horizonOpNav.delete_HorizonOpNavData

# Register HorizonOpNavData in _horizonOpNav:
_horizonOpNav.HorizonOpNavData_swigregister(HorizonOpNavData)


def QRDecomp(inMat, nRow, Q, R):
    return _horizonOpNav.QRDecomp(inMat, nRow, Q, R)

def BackSub(R, inVec, nRow, n):
    return _horizonOpNav.BackSub(R, inVec, nRow, n)

import sys
protectAllClasses(sys.modules[__name__])



