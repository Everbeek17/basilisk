# This file was automatically generated by SWIG (http://www.swig.org).
# Version 4.0.2
#
# Do not make changes to this file unless you know what you are doing--modify
# the SWIG interface file instead.

from sys import version_info as _swig_python_version_info
if _swig_python_version_info < (2, 7, 0):
    raise RuntimeError("Python 2.7 or later required")

# Import the low-level C/C++ module
if __package__ or "." in __name__:
    from . import _hillStateConverter
else:
    import _hillStateConverter

try:
    import builtins as __builtin__
except ImportError:
    import __builtin__

def _swig_repr(self):
    try:
        strthis = "proxy of " + self.this.__repr__()
    except __builtin__.Exception:
        strthis = ""
    return "<%s.%s; %s >" % (self.__class__.__module__, self.__class__.__name__, strthis,)


def _swig_setattr_nondynamic_instance_variable(set):
    def set_instance_attr(self, name, value):
        if name == "thisown":
            self.this.own(value)
        elif name == "this":
            set(self, name, value)
        elif hasattr(self, name) and isinstance(getattr(type(self), name), property):
            set(self, name, value)
        else:
            raise AttributeError("You cannot add instance attributes to %s" % self)
    return set_instance_attr


def _swig_setattr_nondynamic_class_variable(set):
    def set_class_attr(cls, name, value):
        if hasattr(cls, name) and not isinstance(getattr(cls, name), property):
            set(cls, name, value)
        else:
            raise AttributeError("You cannot add class attributes to %s" % cls)
    return set_class_attr


def _swig_add_metaclass(metaclass):
    """Class decorator for adding a metaclass to a SWIG wrapped class - a slimmed down version of six.add_metaclass"""
    def wrapper(cls):
        return metaclass(cls.__name__, cls.__bases__, cls.__dict__.copy())
    return wrapper


class _SwigNonDynamicMeta(type):
    """Meta class to enforce nondynamic attributes (no new attributes) for a class"""
    __setattr__ = _swig_setattr_nondynamic_class_variable(type.__setattr__)



def new_doubleArray(nelements):
    return _hillStateConverter.new_doubleArray(nelements)

def delete_doubleArray(ary):
    return _hillStateConverter.delete_doubleArray(ary)

def doubleArray_getitem(ary, index):
    return _hillStateConverter.doubleArray_getitem(ary, index)

def doubleArray_setitem(ary, index, value):
    return _hillStateConverter.doubleArray_setitem(ary, index, value)

def new_longArray(nelements):
    return _hillStateConverter.new_longArray(nelements)

def delete_longArray(ary):
    return _hillStateConverter.delete_longArray(ary)

def longArray_getitem(ary, index):
    return _hillStateConverter.longArray_getitem(ary, index)

def longArray_setitem(ary, index, value):
    return _hillStateConverter.longArray_setitem(ary, index, value)

def new_intArray(nelements):
    return _hillStateConverter.new_intArray(nelements)

def delete_intArray(ary):
    return _hillStateConverter.delete_intArray(ary)

def intArray_getitem(ary, index):
    return _hillStateConverter.intArray_getitem(ary, index)

def intArray_setitem(ary, index, value):
    return _hillStateConverter.intArray_setitem(ary, index, value)

def new_shortArray(nelements):
    return _hillStateConverter.new_shortArray(nelements)

def delete_shortArray(ary):
    return _hillStateConverter.delete_shortArray(ary)

def shortArray_getitem(ary, index):
    return _hillStateConverter.shortArray_getitem(ary, index)

def shortArray_setitem(ary, index, value):
    return _hillStateConverter.shortArray_setitem(ary, index, value)


def getStructSize(self):
    try:
        return eval('sizeof_' + repr(self).split(';')[0].split('.')[-1])
    except (NameError) as e:
        typeString = 'sizeof_' + repr(self).split(';')[0].split('.')[-1]
        raise NameError(e.message + '\nYou tried to get this size macro: ' + typeString + 
            '\n It appears to be undefined.  \nYou need to run the SWIG GEN_SIZEOF' +  
            ' SWIG macro against the class/struct in your SWIG file if you want to ' + 
            ' make this call.\n')


def protectSetAttr(self, name, value):
    if(hasattr(self, name) or name == 'this'):
        object.__setattr__(self, name, value)
    else:
        raise ValueError('You tried to add this variable: ' + name + '\n' + 
            'To this class: ' + str(self))

def protectAllClasses(moduleType):
    import inspect
    import sys
    clsmembers = inspect.getmembers(sys.modules[__name__], inspect.isclass)
    for member in clsmembers:
        try:
            exec(str(member[0]) + '.__setattr__ = protectSetAttr')
            exec(str(member[0]) + '.getStructSize = getStructSize') 
        except (AttributeError, TypeError) as e:
            pass


Update_hillStateConverter = _hillStateConverter.Update_hillStateConverter
SelfInit_hillStateConverter = _hillStateConverter.SelfInit_hillStateConverter
Reset_hillStateConverter = _hillStateConverter.Reset_hillStateConverter
class NavTransMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    timeTag = property(_hillStateConverter.NavTransMsgPayload_timeTag_get, _hillStateConverter.NavTransMsgPayload_timeTag_set)
    r_BN_N = property(_hillStateConverter.NavTransMsgPayload_r_BN_N_get, _hillStateConverter.NavTransMsgPayload_r_BN_N_set)
    v_BN_N = property(_hillStateConverter.NavTransMsgPayload_v_BN_N_get, _hillStateConverter.NavTransMsgPayload_v_BN_N_set)
    vehAccumDV = property(_hillStateConverter.NavTransMsgPayload_vehAccumDV_get, _hillStateConverter.NavTransMsgPayload_vehAccumDV_set)

    def __init__(self):
        _hillStateConverter.NavTransMsgPayload_swiginit(self, _hillStateConverter.new_NavTransMsgPayload())
    __swig_destroy__ = _hillStateConverter.delete_NavTransMsgPayload

# Register NavTransMsgPayload in _hillStateConverter:
_hillStateConverter.NavTransMsgPayload_swigregister(NavTransMsgPayload)

class HillRelStateMsgPayload(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    r_DC_H = property(_hillStateConverter.HillRelStateMsgPayload_r_DC_H_get, _hillStateConverter.HillRelStateMsgPayload_r_DC_H_set)
    v_DC_H = property(_hillStateConverter.HillRelStateMsgPayload_v_DC_H_get, _hillStateConverter.HillRelStateMsgPayload_v_DC_H_set)

    def __init__(self):
        _hillStateConverter.HillRelStateMsgPayload_swiginit(self, _hillStateConverter.new_HillRelStateMsgPayload())
    __swig_destroy__ = _hillStateConverter.delete_HillRelStateMsgPayload

# Register HillRelStateMsgPayload in _hillStateConverter:
_hillStateConverter.HillRelStateMsgPayload_swigregister(HillRelStateMsgPayload)

class HillStateConverterConfig(object):
    thisown = property(lambda x: x.this.own(), lambda x, v: x.this.own(v), doc="The membership flag")
    __repr__ = _swig_repr
    hillStateOutMsg = property(_hillStateConverter.HillStateConverterConfig_hillStateOutMsg_get, _hillStateConverter.HillStateConverterConfig_hillStateOutMsg_set)
    chiefStateInMsg = property(_hillStateConverter.HillStateConverterConfig_chiefStateInMsg_get, _hillStateConverter.HillStateConverterConfig_chiefStateInMsg_set)
    depStateInMsg = property(_hillStateConverter.HillStateConverterConfig_depStateInMsg_get, _hillStateConverter.HillStateConverterConfig_depStateInMsg_set)
    bskLogger = property(_hillStateConverter.HillStateConverterConfig_bskLogger_get, _hillStateConverter.HillStateConverterConfig_bskLogger_set)

    def __init__(self):
        _hillStateConverter.HillStateConverterConfig_swiginit(self, _hillStateConverter.new_HillStateConverterConfig())
    __swig_destroy__ = _hillStateConverter.delete_HillStateConverterConfig

# Register HillStateConverterConfig in _hillStateConverter:
_hillStateConverter.HillStateConverterConfig_swigregister(HillStateConverterConfig)


import sys
protectAllClasses(sys.modules[__name__])



